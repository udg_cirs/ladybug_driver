#!/bin/bash

if [ "$#" = "1" ]; then
    for dir in $1* ; do
        if [ -d "$dir" ]; then
            # Check if it's a directory
            echo $dir
			echo "Creating $dir.tar.gz ..."
		    $(tar -czf "$dir.tar.gz" -C $1 $(python -c "import os.path; print os.path.relpath('$dir', '$1')"))
        fi
    done
else
    echo "No folder argument"
fi
