/*
 * Program to get the calibration saved inside ladybug camera.
 *
 * Original version written by Josep Bosch <jep250@gmail.com>
 *
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <dc1394/dc1394.h>

int main(int argc, char **argv) {
  dc1394error_t err;
  dc1394_t *d;
  dc1394camera_list_t *list;
  dc1394camera_t *camera;

  d = dc1394_new();
  if (!d)
    return 1;
  err = dc1394_camera_enumerate(d, &list);
  DC1394_ERR_RTN(err, "Failed to enumerate cameras");

  if (list->num == 0) {
    dc1394_log_error("No cameras found");
    return 1;
  }

  camera = dc1394_camera_new(d, list->ids[0].guid);
  if (!camera) {
    dc1394_log_error("Failed to initialize camera with guid %llx",
                     list->ids[0].guid);
    return 1;
  }
  dc1394_camera_free_list(list);
  printf("Using camera \"%s %s\"\n", camera->vendor, camera->model);

  // Saves camera info in a file

  printf("Saving camera info in a file\n");
  FILE *fd = fopen("camerainfo.txt", "w");

  err = dc1394_camera_print_info(camera, fd);
  DC1394_ERR_RTN(err, "Can't save camera info in a file");

  fclose(fd);

  // Saves calibration file
  // this returns about 200kb of data (and it takes some seconds to retrieve it)

  uint32_t val;
  uint32_t reg = 0x1000;

  printf("Saving camera calibration in a file\n");
  FILE *fw = fopen("calibration.txt", "w");
  while ((err = dc1394_get_adv_control_register(camera, reg, &val)) ==
         DC1394_SUCCESS) {
    val = htonl(val); // big endian to little endian
    fwrite(&val, 4, 1, fw);
    reg += 4;
  }
  fclose(fw);

  dc1394_camera_free(camera);
  dc1394_free(d);

  return 0;
}
