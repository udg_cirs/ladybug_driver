#include "ladybug_driver_equi.hpp"
#include <chrono>

// Camera parameters.

#define VIDEO_MODE DC1394_VIDEO_MODE_FORMAT7_7
#define OPERATION_MODE DC1394_OPERATION_MODE_1394B
#define ISO_SPEED DC1394_ISO_SPEED_800
#define BPP 8160
#define DMA_RING_BUFFER 10  // Buffer of images
#define WIDTH 808
#define HEIGHT                                                                                                         \
  5000  // MAX 14784   No more fps smaller than 5000. Increase, decrease related
        // to fps and jpeg quality.

bool finished = false;

// Ctrl-c to stop
void my_handler(int s)
{
  printf("Caught signal %d\n", s);
  finished = true;
}

DriverJpeg::DriverJpeg(int argc, char **argv) : saveImagesThread_(boost::bind(&DriverJpeg::save_images, this))
{
  directory_ = argv[1];  // Relative path to the directory
  time_ = atof(argv[2]);
  buffer_full_ = 0;

  std::cout << "Reading look up table ..." << std::endl;
  if (!readLUT(argv[3]))
  {
    std::cout << "Can't read LUT" << std::endl;
  }
}

bool DriverJpeg::readLUT(char *mesh_file_path)
{
  FILE *fp = fopen(mesh_file_path, "r");
  if (fp == NULL)
  {
    printf("Can't read 3D mesh file: %s\n", mesh_file_path);
    return false;
  }

  if (fscanf(fp, "cols %d rows %d\n", &pano_width_, &pano_height_) != 2)
  {
    printf("Can't read cols/rows in 3d mesh file.\n");
    fclose(fp);
    return false;
  }

  printf("cols: %d, rows: %d\n", pano_width_, pano_height_);

  LUT_ = cv::Mat::ones(pano_height_, pano_width_, CV_32SC3) * -1;

  for (int iRow = 0; iRow < pano_height_; iRow++)
  {
    for (int iCol = 0; iCol < pano_width_; iCol++)
    {
      int R, C;
      int camera;
      if (fscanf(fp, "%d, %d, %d", &R, &C, &camera) != 3)
      {
        printf("Can't read grid data in 3d mesh file.\n");
        printf(" row: %d, col: %d\n", iRow, iCol);
        return false;
      }
      LUT_.ptr<int>(iRow, iCol)[0] = R;
      LUT_.ptr<int>(iRow, iCol)[1] = C;
      LUT_.ptr<int>(iRow, iCol)[2] = camera;
    }
  }
  fclose(fp);
  return true;
}

DriverJpeg::~DriverJpeg()
{
}
int DriverJpeg::run()
{
  dc1394error_t err;
  dc1394camera_t *camera;
  dc1394_t *d;
  // dc1394video_frame_t *frame;
  dc1394camera_list_t *list;

  d = dc1394_new();
  if (!d)
    return 1;
  err = dc1394_camera_enumerate(d, &list);
  DC1394_ERR_RTN(err, "Failed to enumerate cameras");

  if (list->num == 0)
  {
    dc1394_log_error("No cameras found");
    return 1;
  }

  camera = dc1394_camera_new(d, list->ids[0].guid);
  if (!camera)
  {
    dc1394_log_error("Failed to initialize camera with guid %llx", list->ids[0].guid);
    return 1;
  }
  dc1394_camera_free_list(list);
  printf("Using camera \"%s %s\"\n", camera->vendor, camera->model);
  // setup video mode, etc...

  err = dc1394_video_set_operation_mode(camera, OPERATION_MODE);
  DC1394_ERR_RTN(err, "Could not set B mode");
  err = dc1394_video_set_iso_speed(camera, ISO_SPEED);
  DC1394_ERR_RTN(err, "Could not set 800Mbps speed");
  err = dc1394_video_set_mode(camera, VIDEO_MODE);
  DC1394_ERR_RTN(err, "Could not set video mode");

  err = dc1394_format7_set_roi(camera, VIDEO_MODE, DC1394_COLOR_CODING_MONO8, BPP, 0, 0, WIDTH, HEIGHT);
  DC1394_ERR_RTN(err, "Could not set ROI");

  // setup capture
  err = dc1394_capture_setup(camera, DMA_RING_BUFFER, DC1394_CAPTURE_FLAGS_DEFAULT);
  DC1394_ERR_RTN(err, "Could not setup capture");
  err = dc1394_video_set_transmission(camera, DC1394_ON);
  DC1394_ERR_RTN(err, "Could not start transmission");

  double total_time = 0;

  // DEBUG
  bool first_image = true;
  int prev_ladybug_id, id_ladybug;

  while (total_time < time_ & !finished)
  {
    dc1394video_frame_t *frame;
    // capture frame
    dc1394_capture_dequeue(camera, DC1394_CAPTURE_POLICY_WAIT, &frame);
    DC1394_ERR_RTN(err, "Could not dequeue a frame")

    // Save first timestamp as laduybug does not give absoulte time.
    if (first_image)
    {
      time_start_ = (double)frame->timestamp / 1000000;
      first_image = false;
    }

    mtx_.lock();
    // push frame to buffer
    if (buffer_.size() < 100)
    {
      // We need to make a copy of the iamge because it's overwritten in ever
      // cycle of the dma buffer.
      int image_bytes = frame->image_bytes;  // It must be the same than
                                             // specified at the top (808*5000)
      int padding_bytes = frame->padding_bytes;
      unsigned char *image_copy = (unsigned char *)malloc(image_bytes + padding_bytes);
      memcpy(image_copy, frame->image, (image_bytes + padding_bytes));
      buffer_.push_back(image_copy);
    }
    else
    {
      printf("=======WARNING: Buffer is full! Dropping frames =========\n");
      std::cout << "Processing frame: " << nframes_ << std::endl;
      buffer_full_++;
      usleep(10000);
    }
    total_time = total_diff_;
    mtx_.unlock();

    // release frame
    err = dc1394_capture_enqueue(camera, frame);
    DC1394_ERR_RTN(err, "Could not enqueue a frame");
    // free(frame->image);
    // free(frame);
  }
  // stop capture
  err = dc1394_video_set_transmission(camera, DC1394_OFF);
  DC1394_ERR_RTN(err, "Could not stop transmission");
  err = dc1394_capture_stop(camera);
  DC1394_ERR_RTN(err, "Could not stop capture");
  dc1394_camera_free(camera);
  dc1394_free(d);
  printf("Waiting until all images are saved\n");
  saveImagesThread_.join();

  // First frame doesn't count for computations of fps.
  std::cout << "Captured " << nframes_ << " frames in " << total_diff_
            << "sec. Average: " << (nframes_ - 1) / total_diff_ << " fps Max diff Ladybug: " << max_diff_ladybug_
            << "sec." << std::endl;
  std::cout << "Lost frames: " << total_lost_frames_ << ". Due to buffer full: " << buffer_full_ << std::endl;
  return 0;
}
void DriverJpeg::save_images()
{
  double start_ladybug;
  double prev_ladybug;
  total_diff_ = 0;        // Difference of time
  max_diff_ladybug_ = 0;  // Difference of time
  int prev_ladybug_id;
  int lost_frames = 0;
  total_lost_frames_ = 0;
  FILE *fd;

  // dc1394video_frame_t *frame;
  // unsigned char * image;
  char filename[256];
  nframes_ = 0;  // Captured frames.

  // Variables to retrieve directory path.
  long size;
  char *buf;
  char *basename;

  size = pathconf(".", _PC_PATH_MAX);
  if ((buf = (char *)malloc((size_t)size)) != NULL)
    basename = getcwd(buf, (size_t)size);

  // char directory;
  char directoryPath[200];
  // directory="test";

  sprintf(directoryPath, "%s/%s", basename, directory_);

  int status;
  status = mkdir(directoryPath, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  if (status == -1)
  {
    printf("Cannot create directory or directory exists. Please choose another "
           "one or delete it.\n");
    exit(1);
  }

  bool first_image = true;
  int cam = 0;
  unsigned int adr;

  while (true)
  {
    unsigned char *image = NULL;
    mtx_.lock();
    int frames = buffer_.size();
    if (frames > 0)
    {
      image = buffer_.front();
      buffer_.erase(buffer_.begin(), buffer_.begin() + 1);
    }
    mtx_.unlock();
    if (image != NULL)
    {
      // Get Ladybug timestamp
      adr = 0x0018;
      unsigned int seconds = (((unsigned int)*(image + adr)) << 24) + (((unsigned int)*(image + adr + 1)) << 16) +
                             (((unsigned int)*(image + adr + 2)) << 8) + (((unsigned int)*(image + adr + 3)));

      adr = 0x001C;
      unsigned int microseconds = (((unsigned int)*(image + adr)) << 24) + (((unsigned int)*(image + adr + 1)) << 16) +
                                  (((unsigned int)*(image + adr + 2)) << 8) + (((unsigned int)*(image + adr + 3)));
      double timestamp_ladybug = seconds + microseconds / 1000000.0;

      adr = 0x0020;  // Frame id
      int id_ladybug = (int)((((unsigned int)*(image + adr)) << 24) + (((unsigned int)*(image + adr + 1)) << 16) +
                             (((unsigned int)*(image + adr + 2)) << 8) + (((unsigned int)*(image + adr + 3))));

      // Save the image in separated files
      cv::Mat color_images[6];
#pragma omp parallel for default(none) shared(color_images, image, adr)
      for (cam = 0; cam < 6; cam++)
      {
        cv::Mat imgMat[4];
        unsigned int jpgadr, jpgsize, cam_adr;
        for (int k = 0; k < 4; k++)
        {
          cam_adr = 0x340 + cam * 32 + k * 8;
          jpgadr = (((unsigned int)*(image + cam_adr)) << 24) + (((unsigned int)*(image + cam_adr + 1)) << 16) +
                   (((unsigned int)*(image + cam_adr + 2)) << 8) + (((unsigned int)*(image + cam_adr + 3)));
          cam_adr += 4;
          jpgsize = (((unsigned int)*(image + cam_adr)) << 24) + (((unsigned int)*(image + cam_adr + 1)) << 16) +
                    (((unsigned int)*(image + cam_adr + 2)) << 8) + (((unsigned int)*(image + cam_adr + 3)));

          if (jpgsize != 0)
          {
            std::vector<unsigned char> buffer(image + jpgadr, image + jpgadr + jpgsize);
            imgMat[k] = cv::imdecode(buffer, CV_LOAD_IMAGE_GRAYSCALE);
          }
        }
        // Reconstruct the original image from the four channels! RGGB
        cv::Mat color_image = cv::Mat::zeros(1232, 1616, CV_8U);  // height, width
        int x, y;
        for (y = 0; y < 1232; y++)
        {
          // std::cout << y << std::endl;
          uint8_t *row = color_image.ptr<uint8_t>(y);
          if (y % 2 == 0)
          {
            uint8_t *i0 = imgMat[0].ptr<uint8_t>(y / 2);
            uint8_t *i1 = imgMat[1].ptr<uint8_t>(y / 2);

            for (x = 0; x < 1616;)
            {
              // R
              row[x] = i0[x / 2];
              x++;

              // G1
              row[x] = i1[x / 2];
              x++;
            }
          }
          else
          {
            uint8_t *i2 = imgMat[2].ptr<uint8_t>(y / 2);
            uint8_t *i3 = imgMat[3].ptr<uint8_t>(y / 2);

            for (x = 0; x < 1616;)
            {
              // G2
              row[x] = i2[x / 2];
              x++;

              // B
              row[x] = i3[x / 2];
              x++;
            }
          }
        }
        // Debayer
        cvtColor(color_image, color_image, cv::COLOR_BayerBG2BGR);
        color_images[cam] = color_image;
      }

      // Panoramic image
      cv::Mat panoramic_image = cv::Mat::zeros(pano_height_, pano_width_, CV_8UC3);
#pragma omp parallel for default(none) shared(panoramic_image, LUT_, color_images)
      for (int y = 0; y < pano_height_; y++)
      {
        uint8_t *row = panoramic_image.ptr<uint8_t>(y);
        int *row_lut = LUT_.ptr<int>(y);
        for (int x = 0; x != pano_width_; x++)
        {
          int camera = row_lut[x * 3 + 2];
          if (camera != -1)
          {
            cv::Vec3b intensity = color_images[camera].ptr<cv::Vec3b>(row_lut[x * 3])[row_lut[x * 3 + 1]];

            row[x * 3] = intensity[0];
            row[x * 3 + 1] = intensity[1];
            row[x * 3 + 2] = intensity[2];
          }
        }
      }

      // SHOW IMAGE ON SCREEN
      //cv::imshow("Pano", panoramic_image);
      //cv::waitKey(10);

      // SAVE IMAGE ON DISK
      // Save timestamp
      //sprintf(filename, "%s/pano_frame%05d.jpg", directoryPath, nframes_);
      //cv::imwrite(filename, panoramic_image);

      // std::cout <<id_ladybug << std::endl;
      if (first_image)
      {
        start_ladybug = timestamp_ladybug;  // We keep a copy of first (non
                                            // absoulte ladybug timestamp)
        timestamp_ladybug = time_start_;
        first_image = false;
      }
      else
      {
        timestamp_ladybug += time_start_ - start_ladybug;
        lost_frames = id_ladybug - prev_ladybug_id - 1;
        if (lost_frames != 0)
        {
          total_lost_frames_ += lost_frames;
          std::cout << "==============WARNING: " << lost_frames << " FRAMES LOST==============" << std::endl;
        }
        // compute time delay
        double diff_ladybug = timestamp_ladybug - prev_ladybug;
        if (max_diff_ladybug_ < diff_ladybug)
        {
          max_diff_ladybug_ = diff_ladybug;
        }
        // printf("Time elapsed between frames: %f sec\n",diff);
        // printf("Time elapsed between frames ladybug: %f sec\n",diff_ladybug);
      }
      prev_ladybug_id = id_ladybug;

      // Save timestamp
      sprintf(filename, "%s/%s%05d.txt", directoryPath, "frame", nframes_);
      fd = fopen(filename, "w");
      // fprintf(fd,"%lf",time_sec);
      fprintf(fd, "%lf", timestamp_ladybug);
      fclose(fd);

      total_diff_ = timestamp_ladybug - time_start_;
      prev_ladybug = timestamp_ladybug;

      std::cout << std::setw(5) << std::setfill('0') << nframes_ << " Buffer: " << std::setw(5) << std::setfill('0')
                << frames << " (100) \r" << std::flush;
      nframes_++;
      free(image);
    }
    else if (total_diff_ >= time_ || finished)
    {
      return;
    }
    else
    {
      usleep(10000);  // Sleep 10ms
    }
  }
}
int main(int argc, char **argv)
{
  if (argc != 4)
  {
    std::cerr << "Usage :" << argv[0] << " <Directory> <Time> <LookUpTable.txt>" << std::endl;
    std::cerr << "This will save the equirectangular images in the specified folder durng the Time selected."
              << std::endl;
    return 1;
  }

  // Handle ctrl-c
  struct sigaction sigIntHandler;

  sigIntHandler.sa_handler = my_handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;

  sigaction(SIGINT, &sigIntHandler, NULL);

  // Create a new NodeExample object.
  DriverJpeg *object = new DriverJpeg(argc, argv);
  object->run();

  return 0;
}  // end main()
