/*
 * Program to get the calibration saved inside ladybug camera.
 *
 * Original version written by Josep Bosch <jep250@gmail.com>
 *
 */

#include <ctype.h> // isdigit()
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <dc1394/dc1394.h>

bool is_number(const std::string &s) {
  std::string::const_iterator it = s.begin();
  while (it != s.end() && std::isdigit(*it))
    ++it;
  return !s.empty() && it == s.end();
}

void print_help(char **argv) {
  std::cerr << "Usage :" << argv[0] << " <NewShutterValue>" << std::endl;
  std::cerr << "If given a new value will set this shutter value for all the "
               "cameras. If no value given it will show the current one. For "
               "auto instead of value write AUTO"
            << std::endl;
}

int main(int argc, char **argv) {

  if (argc > 2) {
    print_help(argv);
    return 1;
  }

  dc1394error_t err;
  dc1394_t *d;
  dc1394camera_list_t *list;
  dc1394camera_t *camera;
  dc1394switch_t power;
  dc1394feature_mode_t mode;

  d = dc1394_new();
  if (!d)
    return 1;
  err = dc1394_camera_enumerate(d, &list);
  DC1394_ERR_RTN(err, "Failed to enumerate cameras");

  if (list->num == 0) {
    dc1394_log_error("No cameras found");
    return 1;
  }

  camera = dc1394_camera_new(d, list->ids[0].guid);
  if (!camera) {
    dc1394_log_error("Failed to initialize camera with guid %llx",
                     list->ids[0].guid);
    return 1;
  }
  dc1394_camera_free_list(list);
  printf("Using camera \"%s %s\"\n", camera->vendor, camera->model);

  // Gets value of shutter.

  uint32_t val;
  // uint32_t reg = 0x1000;

  printf("Geting info about shutter: \n");

  dc1394_feature_get_power(camera, DC1394_FEATURE_SHUTTER, &power);
  DC1394_ERR_RTN(err, "Failed to get shutter power");

  std::string shutter_power;
  if (power) {
    shutter_power = "ON";
  } else {
    shutter_power = "OFF";
  }
  printf("Power of shutter is: %s \n", shutter_power.c_str());

  dc1394_feature_get_mode(camera, DC1394_FEATURE_SHUTTER, &mode);
  DC1394_ERR_RTN(err, "Failed to get shutter mode");

  std::string shutter_mode;
  if (mode == 736) {
    shutter_mode = "MANUAL";
  } else if (mode == 737) {
    shutter_mode = "AUTO";
  } else if (mode == 738) {
    shutter_mode = "ONE PUSH";
  } else {
    printf("ERROR: Shutter mode not understood.\n");
    return 1;
  }

  printf("Mode of shutter is: %s \n", shutter_mode.c_str());

  err = dc1394_feature_get_value(camera, DC1394_FEATURE_SHUTTER, &val);
  DC1394_ERR_RTN(err, "Failed to get shutter value");
  printf("Value of shutter is: %u \n", val);

  if (argv[1]) {
    if (is_number(std::string(argv[1]))) {
      // Make sure shutter is on
      printf("Setting Shutter to ON\n");
      power = DC1394_ON;
      dc1394_feature_set_power(camera, DC1394_FEATURE_SHUTTER, power);
      DC1394_ERR_RTN(err, "Failed to power on Shutter");
      // Make sure mode is set to manual
      if (shutter_mode.compare("MANUAL") != 0) {
        printf("Setting shutter to MANUAL mode\n");
        mode = DC1394_FEATURE_MODE_MANUAL; // MANUAL
        dc1394_feature_set_mode(camera, DC1394_FEATURE_SHUTTER, mode);
        DC1394_ERR_RTN(err, "Failed to set shutter mode");
      }
      val = (unsigned int)atoi(argv[1]);
      printf("Setting new shutter value to: %u\n", val);
      err = dc1394_feature_set_value(camera, DC1394_FEATURE_SHUTTER, val);
      DC1394_ERR_RTN(err, "Failed to set shutter value");
      err = dc1394_feature_get_value(camera, DC1394_FEATURE_SHUTTER, &val);
      printf("Value of shutter is set now to: %u \n", val);
      DC1394_ERR_RTN(err, "Failed to get shutter value");
    } else if (std::string(argv[1]).compare("AUTO") == 0) {
      // Make sure shutter is on
      printf("Setting Shutter to ON\n");
      power = DC1394_ON;
      dc1394_feature_set_power(camera, DC1394_FEATURE_SHUTTER, power);
      DC1394_ERR_RTN(err, "Failed to power on Shutter");

      printf("Setting shutter to AUTO\n");
      mode = DC1394_FEATURE_MODE_AUTO;
      dc1394_feature_set_mode(camera, DC1394_FEATURE_SHUTTER, mode);
      DC1394_ERR_RTN(err, "Failed to set shutter mode");
    } else {
      print_help(argv);
    }
  }

  dc1394_camera_free(camera);
  dc1394_free(d);

  return 0;
}
