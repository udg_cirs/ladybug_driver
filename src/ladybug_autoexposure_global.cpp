/*
 * Program to get the calibration saved inside ladybug camera.
 *
 * Original version written by Josep Bosch <jep250@gmail.com>
 *
 */

#include <ctype.h> // isdigit()
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <dc1394/dc1394.h>

bool is_number(const std::string &s) {
  std::string::const_iterator it = s.begin();
  while (it != s.end() && std::isdigit(*it))
    ++it;
  return !s.empty() && it == s.end();
}

void print_help(char **argv) {
  std::cerr << "Usage :" << argv[0] << " <NewExposureValue>" << std::endl;
  std::cerr << "If given a new value will set this exposure value for all the "
               "cameras. If no value given it will show the current one. For "
               "auto instead of value write AUTO"
            << std::endl;
}

int main(int argc, char **argv) {

  if (argc > 2) {
    print_help(argv);
    return 1;
  }

  dc1394error_t err;
  dc1394_t *d;
  dc1394camera_list_t *list;
  dc1394camera_t *camera;
  dc1394feature_mode_t mode;
  dc1394switch_t power;

  d = dc1394_new();
  if (!d)
    return 1;
  err = dc1394_camera_enumerate(d, &list);
  DC1394_ERR_RTN(err, "Failed to enumerate cameras");

  if (list->num == 0) {
    dc1394_log_error("No cameras found");
    return 1;
  }

  camera = dc1394_camera_new(d, list->ids[0].guid);
  if (!camera) {
    dc1394_log_error("Failed to initialize camera with guid %llx",
                     list->ids[0].guid);
    return 1;
  }
  dc1394_camera_free_list(list);
  printf("Using camera \"%s %s\"\n", camera->vendor, camera->model);

  // Gets value of autoexposure.

  uint32_t val;

  printf("Geting info about autoexposure: \n");

  dc1394_feature_get_power(camera, DC1394_FEATURE_EXPOSURE, &power);
  DC1394_ERR_RTN(err, "Failed to get autoexposure power");

  std::string exposure_power;
  if (power) {
    exposure_power = "ON";
  } else {
    exposure_power = "OFF";
  }
  printf("Power of autoexposure is: %s \n", exposure_power.c_str());

  dc1394_feature_get_mode(camera, DC1394_FEATURE_EXPOSURE, &mode);
  DC1394_ERR_RTN(err, "Failed to get autoexposure mode");

  std::string exposure_mode;
  if (mode == 736) {
    exposure_mode = "MANUAL";
  } else if (mode == 737) {
    exposure_mode = "AUTO";
  } else if (mode == 738) {
    exposure_mode = "ONE PUSH";
  } else {
    printf("ERROR: Autoexposure mode not understood.\n");
    return 1;
  }

  printf("Mode of autoexposure is: %s \n", exposure_mode.c_str());

  err = dc1394_feature_get_value(camera, DC1394_FEATURE_EXPOSURE, &val);
  DC1394_ERR_RTN(err, "Failed to get autoexposure value");
  printf("Value of autoexposure is: %u \n", val);

  if (argv[1]) {
    if (is_number(std::string(argv[1]))) {
      // Make sure it is on
      if (!power) {
        printf("Setting AutoExposure to ON\n");
        power = DC1394_ON;
        dc1394_feature_set_power(camera, DC1394_FEATURE_EXPOSURE, power);
        DC1394_ERR_RTN(err, "Failed to power on AutoExposure");
      }
      // Make sure mode is set to manual
      if (exposure_mode.compare("MANUAL") != 0) {
        printf("Setting AutoExposure to MANUAL mode\n");
        mode = DC1394_FEATURE_MODE_MANUAL; // MANUAL
        dc1394_feature_set_mode(camera, DC1394_FEATURE_EXPOSURE, mode);
        DC1394_ERR_RTN(err, "Failed to set AutoExposure mode");
      }
      val = (unsigned int)atoi(argv[1]);
      printf("Setting new AutoExposure value to: %u\n", val);
      err = dc1394_feature_set_value(camera, DC1394_FEATURE_EXPOSURE, val);
      DC1394_ERR_RTN(err, "Failed to set AutoExposure value");
      err = dc1394_feature_get_value(camera, DC1394_FEATURE_EXPOSURE, &val);
      printf("Value of AutoExposure is set now to: %u \n", val);
      DC1394_ERR_RTN(err, "Failed to get AutoExposure value");
    } else if (std::string(argv[1]).compare("AUTO") == 0) {
      // Make sure it is on
      if (!power) {
        printf("Setting AutoExposure to ON\n");
        power = DC1394_ON;
        dc1394_feature_set_power(camera, DC1394_FEATURE_EXPOSURE, power);
        DC1394_ERR_RTN(err, "Failed to power on AutoExposure");
      }
      printf("Setting AutoExposure to AUTO\n");
      mode = DC1394_FEATURE_MODE_AUTO;
      dc1394_feature_set_mode(camera, DC1394_FEATURE_EXPOSURE, mode);
      DC1394_ERR_RTN(err, "Failed to set AutoExposure mode");
    } else if (std::string(argv[1]).compare("OFF") == 0) {
      printf("Setting AutoExposure to OFF\n");
      power = DC1394_OFF;
      dc1394_feature_set_power(camera, DC1394_FEATURE_EXPOSURE, power);
      DC1394_ERR_RTN(err, "Failed to power off AutoExposure");
    } else {
      print_help(argv);
    }
  }
  dc1394_camera_free(camera);
  dc1394_free(d);

  return 0;
}
