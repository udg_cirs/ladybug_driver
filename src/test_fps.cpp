/*
 * Program to get the calibration saved inside ladybug camera.
 *
 * Original version written by Josep Bosch <jep250@gmail.com>
 *
 */

#include <ctype.h> // isdigit()
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <dc1394/dc1394.h>

void print_help(char **argv) {
  std::cerr << "Usage :" << argv[0] << " <NewShutterValue>" << std::endl;
  std::cerr << "If given a new value will set this shutter value for all the "
               "cameras. If no value given it will show the current one. For "
               "auto instead of value write AUTO"
            << std::endl;
}

int main(int argc, char **argv) {

  if (argc != 2) {
    print_help(argv);
    return 1;
  }

  float framerate = atof(argv[1]);

  // copy the bits of IEEE*4 32-bit floating point number to unsigned integer.
  uint32_t ui;
  memcpy(&ui, &framerate, sizeof(ui));

  std::cout << "Fps: Current: \t" << ui << std::endl;

  return 0;
}
