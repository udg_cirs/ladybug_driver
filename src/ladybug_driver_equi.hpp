#ifndef DRIVER_EQUI_H
#define DRIVER_EQUI_H

#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <boost/thread.hpp>
#include <iostream>

// Includes libdc1394
#include <dc1394/dc1394.h>

// Includes opencv
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using std::string;

class DriverJpeg
{
public:
  //! Constructor.
  DriverJpeg(int argc, char **argv);

  //! Destructor.
  ~DriverJpeg();

  int run();

private:
  void save_images();

  bool readLUT(char *mesh_file_path);

  char *directory_;

  std::vector<unsigned char *> buffer_;

  boost::thread saveImagesThread_;
  boost::mutex mtx_;
  double time_;
  int nframes_;
  double total_diff_;
  double max_diff_ladybug_;
  int total_lost_frames_;
  int buffer_full_;
  double time_start_;

  cv::Mat LUT_;
  int pano_width_;
  int pano_height_;
};

#endif  // DRIVER_EQUI_H
