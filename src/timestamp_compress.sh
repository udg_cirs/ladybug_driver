#!/bin/bash
$origin=$(pwd)
if [ "$#" = "1" ]; then
    for dir in $1* ; do
		if [ -d "$dir" ]; then
		    # Check if it's a directory
		    echo "Entering $dir"
			for file in $dir/* ; do
				#Only one timestamp for frame
				#echo "$file"
				len=${#file}
				file_beginning="${file:$len-24:7}"
				#echo $file_beginning
				file_end="${file:$len-5:1}"
				#echo $file_end
				if [ "$file_beginning" = "camera0" ] && [ "$file_end" = "0" ]; then
					modification_date=$(date -r $file +%s.%N)
					#echo $modification_date
					len=${#file}
					new_file="${file:0:$len-24}${file:$len-16:10}.txt"
					echo $new_file
					echo $modification_date > $new_file
					modification_date=$(stat -c '%y' $file)
					#echo $modification_date
					echo $modification_date >> $new_file
				fi
			done
			echo "Creating $dir.tar.gz ..."
		    $(tar -czf "$dir.tar.gz" -C $1 $(python -c "import os.path; print os.path.relpath('$dir', '$1')"))
		fi
	done
else
    echo "No folder argument"
fi
