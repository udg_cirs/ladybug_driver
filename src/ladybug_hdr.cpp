/*
 * Program to get the calibration saved inside ladybug camera.
 *
 * Original version written by Josep Bosch <jep250@gmail.com>
 *
 */

#include <bitset>
#include <ctype.h> // isdigit()
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <dc1394/dc1394.h>

#define VIDEO_MODE DC1394_VIDEO_MODE_FORMAT7_7
#define OPERATION_MODE DC1394_OPERATION_MODE_1394B
#define ISO_SPEED DC1394_ISO_SPEED_800
#define BPP 8160
#define DMA_RING_BUFFER 4
#define WIDTH 808
#define HEIGHT                                                                 \
  5000 // MAX 14784   No more fps smaller than 5000. Increase, decrease related
       // to fps

bool is_number(const std::string &s) {
  std::string::const_iterator it = s.begin();
  while (it != s.end() && std::isdigit(*it))
    ++it;
  return !s.empty() && it == s.end();
}

void print_help(char **argv) {
  std::cerr << "Usage :" << argv[0]
            << " <Gain0> <Shutter0>  <Gain1> <Shutter1>  <Gain2> <Shutter2>  "
               "<Gain3> <Shutter3>"
            << std::endl;
  std::cerr << "Without arguments retrieve info about HDR. Wiht 8 argument "
               "sets HDR values."
            << std::endl;
  std::cerr << "Wiht 1 argument (ON/OFF). With an argument (AUTO) tries to set "
               "good range of values for shutter."
            << std::endl;
}

int get_hdr_values(dc1394camera_t *camera, uint32_t shutter_offset,
                   uint32_t *gain_value, uint32_t *shutter_value) {
  dc1394error_t err;
  // Get values of gain and shutter
  uint32_t gain_register_val;
  uint64_t gain_offset = shutter_offset + 0x04;
  err = dc1394_get_control_register(camera, gain_offset, &gain_register_val);
  DC1394_ERR_RTN(err, "Failed to get gain value");
  // Get 12 most right bits.
  *gain_value = gain_register_val & 0xFFF;
  uint32_t shutter_register_val;
  err = dc1394_get_control_register(camera, shutter_offset,
                                    &shutter_register_val);
  DC1394_ERR_RTN(err, "Failed to get shutter value");
  // Get 12 most right bits.
  *shutter_value = shutter_register_val & 0xFFF;
  return 0;
}
int set_hdr_values(dc1394camera_t *camera, uint32_t shutter_offset,
                   uint32_t gain_value, uint32_t shutter_value) {
  dc1394error_t err;
  // Get values of gain and shutter
  uint32_t gain_register_val;
  uint64_t gain_offset = shutter_offset + 0x04;
  err = dc1394_get_control_register(camera, gain_offset, &gain_register_val);
  DC1394_ERR_RTN(err, "Failed to get gain value");
  uint32_t new_gain_register_val = gain_register_val & 0xFFFFF000;
  new_gain_register_val = new_gain_register_val | gain_value;
  err = dc1394_set_control_register(camera, gain_offset, new_gain_register_val);
  DC1394_ERR_RTN(err, "Failed to set gain value");

  uint32_t shutter_register_val;
  err = dc1394_get_control_register(camera, shutter_offset,
                                    &shutter_register_val);
  DC1394_ERR_RTN(err, "Failed to get shutter value");
  uint32_t new_shutter_register_val = shutter_register_val & 0xFFFFF000;
  new_shutter_register_val = new_shutter_register_val | shutter_value;
  err = dc1394_set_control_register(camera, shutter_offset,
                                    new_shutter_register_val);
  DC1394_ERR_RTN(err, "Failed to set gain value");
  return 0;
}

int main(int argc, char **argv) {

  if (argc != 1 and argc != 2 and argc != 9) { // 0 1 or 8 arguments
    print_help(argv);
    return 1;
  }

  dc1394error_t err;
  dc1394_t *d;
  dc1394camera_list_t *list;
  dc1394camera_t *camera;
  dc1394feature_mode_t mode;
  dc1394switch_t power;

  d = dc1394_new();
  if (!d)
    return 1;
  err = dc1394_camera_enumerate(d, &list);
  DC1394_ERR_RTN(err, "Failed to enumerate cameras");

  if (list->num == 0) {
    dc1394_log_error("No cameras found");
    return 1;
  }

  camera = dc1394_camera_new(d, list->ids[0].guid);
  if (!camera) {
    dc1394_log_error("Failed to initialize camera with guid %llx",
                     list->ids[0].guid);
    return 1;
  }
  dc1394_camera_free_list(list);
  printf("Using camera \"%s %s\"\n", camera->vendor, camera->model);

  if (argc == 1) {
    // Retrieve info about HDR
    uint32_t register_val;
    uint64_t hdr_offset = 0x1800;
    std::string hdr_enabled = "OFF";
    err = dc1394_get_control_register(camera, hdr_offset, &register_val);
    DC1394_ERR_RTN(err, "Failed to get hdr value");
    uint32_t hdr_value = register_val & 0x2000000; // get 6th bit
    if (hdr_value == 0x2000000) {
      hdr_enabled = "ON";
    }
    std::cout << "===============HDR===============" << std::endl;
    std::cout << "HDR : Enabled: \t" << hdr_enabled << std::endl;

    // Get HDR 0 values.
    uint32_t shutter_offset = 0x1820;
    uint32_t gain_value;
    uint32_t shutter_value;
    get_hdr_values(camera, shutter_offset, &gain_value, &shutter_value);
    std::cout << "HDR setting 0 \t Gain:\t" << std::dec << gain_value
              << "\t Shutter: \t" << shutter_value << std::endl;
    shutter_offset = 0x1840;
    get_hdr_values(camera, shutter_offset, &gain_value, &shutter_value);
    std::cout << "HDR setting 1 \t Gain:\t" << std::dec << gain_value
              << "\t Shutter: \t" << shutter_value << std::endl;
    shutter_offset = 0x1860;
    get_hdr_values(camera, shutter_offset, &gain_value, &shutter_value);
    std::cout << "HDR setting 2 \t Gain:\t" << std::dec << gain_value
              << "\t Shutter: \t" << shutter_value << std::endl;
    shutter_offset = 0x1880;
    get_hdr_values(camera, shutter_offset, &gain_value, &shutter_value);
    std::cout << "HDR setting 3 \t Gain:\t" << std::dec << gain_value
              << "\t Shutter: \t" << shutter_value << std::endl;

  } else if (argc == 2) {
    if (std::string(argv[1]).compare("ON") == 0) {
      uint32_t register_val;
      uint64_t hdr_offset = 0x1800;
      err = dc1394_get_control_register(camera, hdr_offset, &register_val);
      DC1394_ERR_RTN(err, "Failed to get hdr value");
      uint32_t new_register_val = register_val | 0x82000000; // set 6th bit to
                                                             // on
      err = dc1394_set_control_register(camera, hdr_offset, new_register_val);
      DC1394_ERR_RTN(err, "Failed to set hdr value");
      err = dc1394_get_control_register(camera, hdr_offset, &register_val);
      DC1394_ERR_RTN(err, "Failed to get hdr value");
      std::string hdr_enabled = "OFF";
      uint32_t hdr_value = register_val & 0x2000000; // get 6th bit
      if (hdr_value == 0x2000000) {
        hdr_enabled = "ON";
      }
      std::cout << "===============HDR===============" << std::endl;
      std::cout << "HDR : Enabled: \t" << hdr_enabled << std::endl;
    } else if (std::string(argv[1]).compare("OFF") == 0) {
      uint32_t register_val;
      uint64_t hdr_offset = 0x1800;
      err = dc1394_get_control_register(camera, hdr_offset, &register_val);
      DC1394_ERR_RTN(err, "Failed to get hdr value");
      uint32_t new_register_val =
          register_val & 0xFDFFFFFF; // set 6th bit to off
      err = dc1394_set_control_register(camera, hdr_offset, new_register_val);
      DC1394_ERR_RTN(err, "Failed to set hdr value");
      err = dc1394_get_control_register(camera, hdr_offset, &register_val);
      DC1394_ERR_RTN(err, "Failed to get hdr value");
      std::string hdr_enabled = "OFF";
      uint32_t hdr_value = register_val & 0x2000000; // get 6th bit
      if (hdr_value == 0x2000000) {
        hdr_enabled = "ON";
      }
      std::cout << "===============HDR===============" << std::endl;
      std::cout << "HDR : Enabled: \t" << hdr_enabled << std::endl;

    } else if (std::string(argv[1]).compare("AUTO") == 0) {

      // Power off HDR
      uint32_t register_val;
      uint64_t hdr_offset = 0x1800;
      err = dc1394_get_control_register(camera, hdr_offset, &register_val);
      DC1394_ERR_RTN(err, "Failed to get hdr value");
      uint32_t new_register_val =
          register_val & 0xFDFFFFFF; // set 6th bit to off
      err = dc1394_set_control_register(camera, hdr_offset, new_register_val);
      DC1394_ERR_RTN(err, "Failed to set hdr value");

      // Set camera to individual exposure mode for a second.
      dc1394switch_t power;
      power = DC1394_OFF;
      dc1394_feature_set_power(camera, DC1394_FEATURE_SHUTTER, power);
      DC1394_ERR_RTN(err, "Failed to power off Shutter");
      power = DC1394_OFF;
      dc1394_feature_set_power(camera, DC1394_FEATURE_GAIN, power);
      DC1394_ERR_RTN(err, "Failed to power off Gain");

      err = dc1394_video_set_operation_mode(camera, OPERATION_MODE);
      DC1394_ERR_RTN(err, "Could not set B mode");
      err = dc1394_video_set_iso_speed(camera, ISO_SPEED);
      DC1394_ERR_RTN(err, "Could not set 800Mbps speed");
      err = dc1394_video_set_mode(camera, VIDEO_MODE);
      DC1394_ERR_RTN(err, "Could not set video mode");

      err =
          dc1394_format7_set_roi(camera, VIDEO_MODE, DC1394_COLOR_CODING_MONO8,
                                 BPP, 0, 0, WIDTH, HEIGHT);
      DC1394_ERR_RTN(err, "Could not set ROI");

      // setup capture
      err = dc1394_capture_setup(camera, DMA_RING_BUFFER,
                                 DC1394_CAPTURE_FLAGS_DEFAULT);
      DC1394_ERR_RTN(err, "Could not setup capture");
      err = dc1394_video_set_transmission(camera, DC1394_ON);
      DC1394_ERR_RTN(err, "Could not start transmission");

      // Wait just a moment for values to get properly set.
      usleep(400000); // wait ms

      // stop capture
      err = dc1394_video_set_transmission(camera, DC1394_OFF);
      DC1394_ERR_RTN(err, "Could not stop transmission");
      err = dc1394_capture_stop(camera);
      DC1394_ERR_RTN(err, "Could not stop capture");

      //============CAMERA2=====================
      uint32_t gain_register_val;
      uint64_t gain_offset = 0x1B40;
      err =
          dc1394_get_control_register(camera, gain_offset, &gain_register_val);
      DC1394_ERR_RTN(err, "Failed to get gain value");
      // Get 12 most right bits.
      uint32_t gain_value_2 = gain_register_val & 0xFFF;
      uint32_t shutter_register_val;
      uint64_t shutter_offset = 0x1B44;
      err = dc1394_get_control_register(camera, shutter_offset,
                                        &shutter_register_val);
      DC1394_ERR_RTN(err, "Failed to get shutter value");
      uint32_t shutter_value_2 = shutter_register_val & 0xFFF;
      //============CAMERA5=====================
      gain_offset = 0x1BA0;
      err =
          dc1394_get_control_register(camera, gain_offset, &gain_register_val);
      DC1394_ERR_RTN(err, "Failed to get gain value");
      // Get 12 most right bits.
      uint32_t gain_value_5 = gain_register_val & 0xFFF;
      shutter_offset = 0x1BA4;
      err = dc1394_get_control_register(camera, shutter_offset,
                                        &shutter_register_val);
      DC1394_ERR_RTN(err, "Failed to get shutter value");
      // Get 12 most right bits.
      uint32_t shutter_value_5 = shutter_register_val & 0xFFF;

      // Set HDR0 as camera2 values.
      shutter_offset = 0x1820;
      uint32_t temp_gain =
          (unsigned int)(((float)gain_value_5 - (float)gain_value_2) * (-0.25) +
                         (float)gain_value_2);
      uint32_t temp_shutter =
          (unsigned int)(((float)shutter_value_5 - (float)shutter_value_2) *
                             (-0.25) +
                         (float)shutter_value_2);
      set_hdr_values(camera, shutter_offset, temp_gain, temp_shutter);

      // Set HDR1 as mid value
      temp_gain = (unsigned int)(((float)gain_value_5 - (float)gain_value_2) *
                                     (-0.25 + 1.5 / 3) +
                                 (float)gain_value_2);
      temp_shutter =
          (unsigned int)(((float)shutter_value_5 - (float)shutter_value_2) *
                             (-0.25 + 1.5 / 3) +
                         (float)shutter_value_2);
      shutter_offset = 0x1840;
      set_hdr_values(camera, shutter_offset, temp_gain, temp_shutter);

      // Set HDR2 as mid value
      temp_gain = (unsigned int)(((float)gain_value_5 - (float)gain_value_2) *
                                     (-0.25 + 1.5 / 3 * 2) +
                                 (float)gain_value_2);
      temp_shutter =
          (unsigned int)(((float)shutter_value_5 - (float)shutter_value_2) *
                             (-0.25 + 1.5 / 3 * 2) +
                         (float)shutter_value_2);
      shutter_offset = 0x1860;
      set_hdr_values(camera, shutter_offset, temp_gain, temp_shutter);
      // Set HDR3 as camera5 values.
      temp_gain =
          (unsigned int)(((float)gain_value_5 - (float)gain_value_2) * 1.25 +
                         (float)gain_value_2);
      temp_shutter =
          (unsigned int)(((float)shutter_value_5 - (float)shutter_value_2) *
                             1.25 +
                         (float)shutter_value_2);
      shutter_offset = 0x1880;
      set_hdr_values(camera, shutter_offset, temp_gain, temp_shutter);
      std::cout << "HDR values set to values between autoexpsure values for "
                   "camera 0 and 5."
                << std::endl;

      hdr_offset = 0x1800;
      err = dc1394_get_control_register(camera, hdr_offset, &register_val);
      DC1394_ERR_RTN(err, "Failed to get hdr value");
      new_register_val = register_val | 0x82000000; // set 6th bit to on
      err = dc1394_set_control_register(camera, hdr_offset, new_register_val);
      DC1394_ERR_RTN(err, "Failed to set hdr value");
      err = dc1394_get_control_register(camera, hdr_offset, &register_val);
      DC1394_ERR_RTN(err, "Failed to get hdr value");
      std::string hdr_enabled = "OFF";
      uint32_t hdr_value = register_val & 0x2000000; // get 6th bit
      if (hdr_value == 0x2000000) {
        hdr_enabled = "ON";
      }
      std::cout << "===============HDR===============" << std::endl;
      std::cout << "HDR : Enabled: \t" << hdr_enabled << std::endl;

    } else {
      print_help(argv);
    }
  } else if (argc == 9 and is_number(argv[2]) and is_number(argv[3]) and
             is_number(argv[4]) and is_number(argv[5]) and
             is_number(argv[6]) and is_number(argv[7]) and is_number(argv[8])) {
    // Set HDR values
    // Set HDR 0 values.
    uint32_t shutter_offset = 0x1820;
    uint32_t gain_value = (unsigned int)atoi(argv[1]);
    uint32_t shutter_value = (unsigned int)atoi(argv[2]);
    set_hdr_values(camera, shutter_offset, gain_value, shutter_value);
    // Set HDR 1 values.
    shutter_offset = 0x1840;
    gain_value = (unsigned int)atoi(argv[3]);
    shutter_value = (unsigned int)atoi(argv[4]);
    set_hdr_values(camera, shutter_offset, gain_value, shutter_value);
    // Set HDR 2 values.
    shutter_offset = 0x1860;
    gain_value = (unsigned int)atoi(argv[5]);
    shutter_value = (unsigned int)atoi(argv[6]);
    set_hdr_values(camera, shutter_offset, gain_value, shutter_value);
    // Set HDR 3 values.
    shutter_offset = 0x1880;
    gain_value = (unsigned int)atoi(argv[7]);
    shutter_value = (unsigned int)atoi(argv[8]);
    set_hdr_values(camera, shutter_offset, gain_value, shutter_value);
    std::cout << "Values were set. " << std::endl;
  } else {
    print_help(argv);
  }

  dc1394_camera_free(camera);
  dc1394_free(d);

  return 0;
}
