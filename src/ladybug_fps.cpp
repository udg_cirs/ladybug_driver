/*
 * Program to get the calibration saved inside ladybug camera.
 *
 * Original version written by Josep Bosch <jep250@gmail.com>
 *
 */

#include <ctype.h> // isdigit()
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <dc1394/dc1394.h>

bool is_number(const std::string &s) {
  std::string::const_iterator it = s.begin();
  while (it != s.end() && std::isdigit(*it))
    ++it;
  return !s.empty() && it == s.end();
}

void print_help(char **argv) {
  std::cerr << "Usage :" << argv[0] << " <NewShutterValue>" << std::endl;
  std::cerr << "If given a new value will set this shutter value for all the "
               "cameras. If no value given it will show the current one. For "
               "auto instead of value write AUTO"
            << std::endl;
}

int main(int argc, char **argv) {

  if (argc > 2) {
    print_help(argv);
    return 1;
  }

  dc1394error_t err;
  dc1394_t *d;
  dc1394camera_list_t *list;
  dc1394camera_t *camera;
  dc1394feature_mode_t mode;

  d = dc1394_new();
  if (!d)
    return 1;
  err = dc1394_camera_enumerate(d, &list);
  DC1394_ERR_RTN(err, "Failed to enumerate cameras");

  if (list->num == 0) {
    dc1394_log_error("No cameras found");
    return 1;
  }

  camera = dc1394_camera_new(d, list->ids[0].guid);
  if (!camera) {
    dc1394_log_error("Failed to initialize camera with guid %llx",
                     list->ids[0].guid);
    return 1;
  }
  dc1394_camera_free_list(list);
  printf("Using camera \"%s %s\"\n", camera->vendor, camera->model);

  if (argc == 1) {
    // Retrieve info about absolute values.
    uint32_t abs_fps_val;
    float min_f, max_f, value_f;
    uint64_t abs_fps_offset = 0x968;
    err = dc1394_get_control_register(camera, abs_fps_offset, &abs_fps_val);
    DC1394_ERR_RTN(err, "Failed to get fps absolute values");
    memcpy(&value_f, &abs_fps_val, sizeof(abs_fps_val));
    err = dc1394_get_control_register(camera, abs_fps_offset - 0x04,
                                      &abs_fps_val);
    DC1394_ERR_RTN(err, "Failed to get fps absolute values");
    memcpy(&max_f, &abs_fps_val, sizeof(abs_fps_val));
    err = dc1394_get_control_register(camera, abs_fps_offset - 0x08,
                                      &abs_fps_val);
    DC1394_ERR_RTN(err, "Failed to get fps absolute values");
    memcpy(&min_f, &abs_fps_val, sizeof(abs_fps_val));

    std::cout << "============ABSOLUTE VALUES============" << std::endl;
    std::cout << "Fps: Current: \t" << value_f << "\t Max: \t" << max_f
              << "\t Min: \t" << min_f << std::endl;
  } else if (is_number(argv[1])) {
    float framerate = atof(argv[1]);
    uint64_t abs_fps_offset = 0x968;
    uint32_t abs_fps_val;
    float min_f, max_f, value_f;
    // Control frame rate with Absolute value control register
    uint32_t value = 0;
    value = 0xC2000000;
    // 0x83 is frame rate control
    err = dc1394_set_control_register(camera, 0x83C, value);
    DC1394_ERR_RTN(err, "Cannot set the frame rate register value");

    // copy the bits of IEEE*4 32-bit floating point number to unsigned integer.
    uint32_t ui;
    memcpy(&ui, &framerate, sizeof(ui));
    err = dc1394_set_control_register(camera, abs_fps_offset, ui);
    DC1394_ERR_RTN(err, "Failed to set fps absolute values");

    // Check it has set properly
    err = dc1394_get_control_register(camera, abs_fps_offset, &abs_fps_val);
    DC1394_ERR_RTN(err, "Failed to get fps absolute values");
    memcpy(&value_f, &abs_fps_val, sizeof(abs_fps_val));
    err = dc1394_get_control_register(camera, abs_fps_offset - 0x04,
                                      &abs_fps_val);
    DC1394_ERR_RTN(err, "Failed to get fps absolute values");
    memcpy(&max_f, &abs_fps_val, sizeof(abs_fps_val));
    err = dc1394_get_control_register(camera, abs_fps_offset - 0x08,
                                      &abs_fps_val);
    DC1394_ERR_RTN(err, "Failed to get fps absolute values");
    memcpy(&min_f, &abs_fps_val, sizeof(abs_fps_val));

    std::cout << "============ABSOLUTE VALUES============" << std::endl;
    std::cout << "Fps: Current: \t" << value_f << "\t Max: \t" << max_f
              << "\t Min: \t" << min_f << std::endl;
  }
  dc1394_camera_free(camera);
  dc1394_free(d);

  return 0;
}
