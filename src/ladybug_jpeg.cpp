/*
 * Program to get the calibration saved inside ladybug camera.
 *
 * Original version written by Josep Bosch <jep250@gmail.com>
 *
 */

#include <ctype.h> // isdigit()
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <dc1394/dc1394.h>

bool is_number(const std::string &s) {
  std::string::const_iterator it = s.begin();
  while (it != s.end() && std::isdigit(*it))
    ++it;
  return !s.empty() && it == s.end();
}

void print_help(char **argv) {
  std::cerr << "Usage :" << argv[0] << " <NewShutterValue>" << std::endl;
  std::cerr << "If given a new value will set this shutter value for all the "
               "cameras. If no value given it will show the current one. For "
               "auto instead of value write AUTO"
            << std::endl;
}

int main(int argc, char **argv) {

  if (argc > 2) {
    print_help(argv);
    return 1;
  }

  dc1394error_t err;
  dc1394_t *d;
  dc1394camera_list_t *list;
  dc1394camera_t *camera;
  dc1394feature_mode_t mode;

  d = dc1394_new();
  if (!d)
    return 1;
  err = dc1394_camera_enumerate(d, &list);
  DC1394_ERR_RTN(err, "Failed to enumerate cameras");

  if (list->num == 0) {
    dc1394_log_error("No cameras found");
    return 1;
  }

  camera = dc1394_camera_new(d, list->ids[0].guid);
  if (!camera) {
    dc1394_log_error("Failed to initialize camera with guid %llx",
                     list->ids[0].guid);
    return 1;
  }
  dc1394_camera_free_list(list);
  printf("Using camera \"%s %s\"\n", camera->vendor, camera->model);

  if (argc == 1) {
    // Retrieve info about jpeg compression.
    uint32_t register_val;
    uint64_t jpeg_offset = 0x1E80;
    err = dc1394_get_control_register(camera, jpeg_offset, &register_val);
    DC1394_ERR_RTN(err, "Failed to get jpeg compression rate");
    uint32_t mode_value = register_val & 0x2000000;
    if (mode_value == 0) {
      std::cout << "Jpeg: \tON" << std::endl;
    } else if (mode_value == 1) {
      std::cout << "Jpeg: \tOFF" << std::endl;
    }
    mode_value = register_val & 0x1000000;
    if (mode_value == 0) {
      std::cout << "Jpeg mode: \tMANUAL" << std::endl;
    } else if (mode_value == 0x1000000) {
      std::cout << "Jpeg mode: \tAUTO" << std::endl;
    }
    uint32_t rate_value = register_val & 0xFFF;
    std::cout << "Jpeg compression rate: \t" << rate_value << std::endl;

  } else if (is_number(argv[1])) {
    // set manual mode
    uint32_t register_val;
    uint64_t jpeg_offset = 0x1E80;
    err = dc1394_get_control_register(camera, jpeg_offset, &register_val);
    DC1394_ERR_RTN(err, "Failed to get jpeg compression rate");
    uint32_t new_value = register_val & 0xFEFFFFFF; // Set to manual
    err = dc1394_set_control_register(camera, jpeg_offset, new_value);
    DC1394_ERR_RTN(err, "Failed to set jpeg compression rate");
    err = dc1394_get_control_register(camera, jpeg_offset, &register_val);
    DC1394_ERR_RTN(err, "Failed to get jpeg compression rate");
    uint32_t mode_value = register_val & 0x1000000;
    if (mode_value == 0) {
      std::cout << "Jpeg mode: \tMANUAL" << std::endl;
    } else if (mode_value == 0x1000000) {
      std::cout << "Jpeg mode: \tAUTO" << std::endl;
    }
    // Set value
    new_value = (unsigned int)atoi(argv[1]);
    uint32_t rate_value = (register_val & 0xFFFFFF00) |
                          new_value; // First set value to zero and then set.
    err = dc1394_set_control_register(camera, jpeg_offset, rate_value);
    DC1394_ERR_RTN(err, "Failed to set jpeg compression rate");
    err = dc1394_get_control_register(camera, jpeg_offset, &register_val);
    DC1394_ERR_RTN(err, "Failed to get jpeg compression rate");
    mode_value = register_val & 0x2000000;
    if (mode_value == 0) {
      std::cout << "Jpeg: \tON" << std::endl;
    } else if (mode_value == 1) {
      std::cout << "Jpeg: \tOFF" << std::endl;
    }
    mode_value = register_val & 0x1000000;
    if (mode_value == 0) {
      std::cout << "Jpeg mode: \tMANUAL" << std::endl;
    } else if (mode_value == 0x1000000) {
      std::cout << "Jpeg mode: \tAUTO" << std::endl;
    }
    rate_value = register_val & 0xFFF;
    std::cout << "Jpeg compression rate: \t" << std::dec << rate_value
              << std::endl;

  } else if (std::string(argv[1]).compare("AUTO") == 0) {
    // Set auto mode
    uint32_t register_val;
    uint64_t jpeg_offset = 0x1E80;
    err = dc1394_get_control_register(camera, jpeg_offset, &register_val);
    DC1394_ERR_RTN(err, "Failed to get jpeg compression rate");
    uint32_t new_value = register_val | 0x1000000; // Set to auto
    err = dc1394_set_control_register(camera, jpeg_offset, new_value);
    DC1394_ERR_RTN(err, "Failed to set jpeg compression rate");
    err = dc1394_get_control_register(camera, jpeg_offset, &register_val);
    DC1394_ERR_RTN(err, "Failed to get jpeg compression rate");
    uint32_t mode_value = register_val & 0x1000000;
    if (mode_value == 0) {
      std::cout << "Jpeg mode: \tMANUAL" << std::endl;
    } else if (mode_value == 0x1000000) {
      std::cout << "Jpeg mode: \tAUTO" << std::endl;
    }
  } else if (std::string(argv[1]).compare("MANUAL") == 0) {
    // Set manual mode
    uint32_t register_val;
    uint64_t jpeg_offset = 0x1E80;
    err = dc1394_get_control_register(camera, jpeg_offset, &register_val);
    DC1394_ERR_RTN(err, "Failed to get jpeg compression rate");
    uint32_t new_value = register_val & 0xFEFFFFFF; // Set to manual
    err = dc1394_set_control_register(camera, jpeg_offset, new_value);
    DC1394_ERR_RTN(err, "Failed to set jpeg compression rate");
    err = dc1394_get_control_register(camera, jpeg_offset, &register_val);
    DC1394_ERR_RTN(err, "Failed to get jpeg compression rate");
    uint32_t mode_value = register_val & 0x1000000;
    if (mode_value == 0) {
      std::cout << "Jpeg mode: \tMANUAL" << std::endl;
    } else if (mode_value == 0x1000000) {
      std::cout << "Jpeg mode: \tAUTO" << std::endl;
    }
  }
  dc1394_camera_free(camera);
  dc1394_free(d);

  return 0;
}
