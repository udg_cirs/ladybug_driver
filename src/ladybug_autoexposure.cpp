/*
 * Program to get the calibration saved inside ladybug camera.
 *
 * Original version written by Josep Bosch <jep250@gmail.com>
 *
 */

#include <ctype.h> // isdigit()
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <dc1394/dc1394.h>

bool is_number(const std::string &s) {
  std::string::const_iterator it = s.begin();
  while (it != s.end() && std::isdigit(*it))
    ++it;
  return !s.empty() && it == s.end();
}

void print_help(char **argv) {
  std::cerr << "Usage :" << argv[0] << " <INDIVIDUAL/GLOBAL>" << std::endl;
  std::cerr << "If no given arguments it will show current values. If given "
               "INDIVIDUAL or GLOBAL will set that autoexposure mode."
            << std::endl;
}

int main(int argc, char **argv) {

  if (argc > 2) {
    print_help(argv);
    return 1;
  }

  dc1394error_t err;
  dc1394_t *d;
  dc1394camera_list_t *list;
  dc1394camera_t *camera;
  dc1394feature_mode_t mode;
  dc1394switch_t power;

  d = dc1394_new();
  if (!d)
    return 1;
  err = dc1394_camera_enumerate(d, &list);
  DC1394_ERR_RTN(err, "Failed to enumerate cameras");

  if (list->num == 0) {
    dc1394_log_error("No cameras found");
    return 1;
  }

  camera = dc1394_camera_new(d, list->ids[0].guid);
  if (!camera) {
    dc1394_log_error("Failed to initialize camera with guid %llx",
                     list->ids[0].guid);
    return 1;
  }
  dc1394_camera_free_list(list);
  printf("Using camera \"%s %s\"\n", camera->vendor, camera->model);

  /*
      //Gets value of autoexposure.

          uint32_t val;

      printf("Geting info about autoexposure: \n");


      dc1394_feature_get_power(camera, DC1394_FEATURE_EXPOSURE, & power);
      DC1394_ERR_RTN(err,"Failed to get autoexposure power");

      std::string exposure_power;
          if(power){
                  exposure_power="ON";
          }
          else{
                  exposure_power="OFF";
          }
          printf("Power of autoexposure is: %s \n", exposure_power.c_str());

          dc1394_feature_get_mode(camera, DC1394_FEATURE_EXPOSURE, & mode);
      DC1394_ERR_RTN(err,"Failed to get autoexposure mode");

          std::string exposure_mode;
          if(mode==736){
                  exposure_mode="MANUAL";
          }
          else if(mode==737){
                  exposure_mode="AUTO";
          }
          else if(mode==738){
                  exposure_mode="ONE PUSH";
          }
          else{
                  printf("ERROR: Autoexposure mode not understood.\n");
                  return 1;
          }

          printf("Mode of autoexposure is: %s \n", exposure_mode.c_str());


          err=dc1394_feature_get_value(camera, DC1394_FEATURE_EXPOSURE, &val);
      DC1394_ERR_RTN(err,"Failed to get autoexposure value");
          printf("Value of autoexposure is: %u \n", val);
  */
  if (argc == 1) {
    // Retrieve info for each one of the cameras.
    std::cout << "============INDIVIDUAL (RELATIVE) VALUES============"
              << std::endl;
    //============CAMERA0=====================
    uint32_t gain_register_val;
    uint64_t gain_offset = 0x1B00;
    err = dc1394_get_control_register(camera, gain_offset, &gain_register_val);
    DC1394_ERR_RTN(err, "Failed to get gain value");
    // Get 12 most right bits.
    uint32_t gain_value;
    gain_value = gain_register_val & 0xFFF;
    uint32_t shutter_register_val;
    uint64_t shutter_offset = 0x1B04;
    err = dc1394_get_control_register(camera, shutter_offset,
                                      &shutter_register_val);
    DC1394_ERR_RTN(err, "Failed to get shutter value");
    // Get 12 most right bits.
    uint32_t shutter_value;
    shutter_value = shutter_register_val & 0xFFF;
    uint32_t exposure_register_val;
    uint64_t exposure_offset = 0x1B08;
    err = dc1394_get_control_register(camera, exposure_offset,
                                      &exposure_register_val);
    DC1394_ERR_RTN(err, "Failed to get exposure value");
    // Get 12 most right bits.
    uint32_t exposure_value;
    exposure_value = exposure_register_val & 0xFFF;
    std::cout << "CAMERA 0 \t Gain:\t" << std::dec << gain_value
              << "\t Shutter: \t" << shutter_value << "\t Exposure: \t"
              << exposure_value << std::endl;

    //============CAMERA1=====================
    gain_offset = 0x1B20;
    err = dc1394_get_control_register(camera, gain_offset, &gain_register_val);
    DC1394_ERR_RTN(err, "Failed to get gain value");
    // Get 12 most right bits.
    gain_value = gain_register_val & 0xFFF;
    shutter_offset = 0x1B24;
    err = dc1394_get_control_register(camera, shutter_offset,
                                      &shutter_register_val);
    DC1394_ERR_RTN(err, "Failed to get shutter value");
    // Get 12 most right bits.
    shutter_value = shutter_register_val & 0xFFF;
    exposure_offset = 0x1B28;
    err = dc1394_get_control_register(camera, exposure_offset,
                                      &exposure_register_val);
    DC1394_ERR_RTN(err, "Failed to get exposure value");
    // Get 12 most right bits.
    exposure_value = exposure_register_val & 0xFFF;
    std::cout << "CAMERA 1 \t Gain:\t" << std::dec << gain_value
              << "\t Shutter: \t" << shutter_value << "\t Exposure: \t"
              << exposure_value << std::endl;

    //============CAMERA2=====================
    gain_offset = 0x1B40;
    err = dc1394_get_control_register(camera, gain_offset, &gain_register_val);
    DC1394_ERR_RTN(err, "Failed to get gain value");
    // Get 12 most right bits.
    gain_value = gain_register_val & 0xFFF;
    shutter_offset = 0x1B44;
    err = dc1394_get_control_register(camera, shutter_offset,
                                      &shutter_register_val);
    DC1394_ERR_RTN(err, "Failed to get shutter value");
    // Get 12 most right bits.
    shutter_value = shutter_register_val & 0xFFF;
    exposure_offset = 0x1B48;
    err = dc1394_get_control_register(camera, exposure_offset,
                                      &exposure_register_val);
    DC1394_ERR_RTN(err, "Failed to get exposure value");
    // Get 12 most right bits.
    exposure_value = exposure_register_val & 0xFFF;
    std::cout << "CAMERA 2 \t Gain:\t" << std::dec << gain_value
              << "\t Shutter: \t" << shutter_value << "\t Exposure: \t"
              << exposure_value << std::endl;
    //============CAMERA3=====================
    gain_offset = 0x1B60;
    err = dc1394_get_control_register(camera, gain_offset, &gain_register_val);
    DC1394_ERR_RTN(err, "Failed to get gain value");
    // Get 12 most right bits.
    gain_value = gain_register_val & 0xFFF;
    shutter_offset = 0x1B64;
    err = dc1394_get_control_register(camera, shutter_offset,
                                      &shutter_register_val);
    DC1394_ERR_RTN(err, "Failed to get shutter value");
    // Get 12 most right bits.
    shutter_value = shutter_register_val & 0xFFF;
    exposure_offset = 0x1B68;
    err = dc1394_get_control_register(camera, exposure_offset,
                                      &exposure_register_val);
    DC1394_ERR_RTN(err, "Failed to get exposure value");
    // Get 12 most right bits.
    exposure_value = exposure_register_val & 0xFFF;
    std::cout << "CAMERA 3 \t Gain:\t" << std::dec << gain_value
              << "\t Shutter: \t" << shutter_value << "\t Exposure: \t"
              << exposure_value << std::endl;
    //============CAMERA4=====================
    gain_offset = 0x1B80;
    err = dc1394_get_control_register(camera, gain_offset, &gain_register_val);
    DC1394_ERR_RTN(err, "Failed to get gain value");
    // Get 12 most right bits.
    gain_value = gain_register_val & 0xFFF;
    shutter_offset = 0x1B84;
    err = dc1394_get_control_register(camera, shutter_offset,
                                      &shutter_register_val);
    DC1394_ERR_RTN(err, "Failed to get shutter value");
    // Get 12 most right bits.
    shutter_value = shutter_register_val & 0xFFF;
    exposure_offset = 0x1B88;
    err = dc1394_get_control_register(camera, exposure_offset,
                                      &exposure_register_val);
    DC1394_ERR_RTN(err, "Failed to get exposure value");
    // Get 12 most right bits.
    exposure_value = exposure_register_val & 0xFFF;
    std::cout << "CAMERA 4 \t Gain:\t" << std::dec << gain_value
              << "\t Shutter: \t" << shutter_value << "\t Exposure: \t"
              << exposure_value << std::endl;
    //============CAMERA5=====================
    gain_offset = 0x1BA0;
    err = dc1394_get_control_register(camera, gain_offset, &gain_register_val);
    DC1394_ERR_RTN(err, "Failed to get gain value");
    // Get 12 most right bits.
    gain_value = gain_register_val & 0xFFF;
    shutter_offset = 0x1BA4;
    err = dc1394_get_control_register(camera, shutter_offset,
                                      &shutter_register_val);
    DC1394_ERR_RTN(err, "Failed to get shutter value");
    // Get 12 most right bits.
    shutter_value = shutter_register_val & 0xFFF;
    exposure_offset = 0x1BA8;
    err = dc1394_get_control_register(camera, exposure_offset,
                                      &exposure_register_val);
    DC1394_ERR_RTN(err, "Failed to get exposure value");
    // Get 12 most right bits.
    exposure_value = exposure_register_val & 0xFFF;
    std::cout << "CAMERA 5 \t Gain:\t" << std::dec << gain_value
              << "\t Shutter: \t" << shutter_value << "\t Exposure: \t"
              << exposure_value << std::endl;

    // Retrieve info about absolute values.
    uint32_t abs_shutter_val;
    float min_f, max_f, value_f;
    uint64_t abs_shutter_offset = 0x918;
    err = dc1394_get_control_register(camera, abs_shutter_offset,
                                      &abs_shutter_val);
    DC1394_ERR_RTN(err, "Failed to get shutter absolute values");
    memcpy(&value_f, &abs_shutter_val, sizeof(abs_shutter_val));
    err = dc1394_get_control_register(camera, abs_shutter_offset - 0x04,
                                      &abs_shutter_val);
    DC1394_ERR_RTN(err, "Failed to get shutter absolute values");
    memcpy(&max_f, &abs_shutter_val, sizeof(abs_shutter_val));
    err = dc1394_get_control_register(camera, abs_shutter_offset - 0x08,
                                      &abs_shutter_val);
    DC1394_ERR_RTN(err, "Failed to get shutter absolute values");
    memcpy(&min_f, &abs_shutter_val, sizeof(abs_shutter_val));

    std::cout << "============ABSOLUTE VALUES============" << std::endl;
    std::cout << "SHUTTER [Sec]: Current: \t" << value_f << "\t Max: \t"
              << max_f << "\t Min: \t" << min_f << std::endl;

    uint32_t abs_gain_val;
    uint64_t abs_gain_offset = 0x928;
    err = dc1394_get_control_register(camera, abs_gain_offset, &abs_gain_val);
    DC1394_ERR_RTN(err, "Failed to get gain absolute values");
    memcpy(&value_f, &abs_gain_val, sizeof(abs_shutter_val));
    err = dc1394_get_control_register(camera, abs_gain_offset - 0x04,
                                      &abs_gain_val);
    DC1394_ERR_RTN(err, "Failed to get gain absolute values");
    memcpy(&max_f, &abs_gain_val, sizeof(abs_shutter_val));
    err = dc1394_get_control_register(camera, abs_gain_offset - 0x08,
                                      &abs_gain_val);
    DC1394_ERR_RTN(err, "Failed to get gain absolute values");
    memcpy(&min_f, &abs_gain_val, sizeof(abs_shutter_val));

    std::cout << "GAIN [dB]: Current: \t" << value_f << "\t Max: \t" << max_f
              << "\t Min: \t" << min_f << std::endl;

  } else if (argc == 2) {
    if (std::string(argv[1]).compare("INDIVIDUAL") == 0) {
      printf("Setting AutoExposure to OFF\n");
      power = DC1394_OFF;
      dc1394_feature_set_power(camera, DC1394_FEATURE_EXPOSURE, power);
      DC1394_ERR_RTN(err, "Failed to power off AutoExposure");

      // Make sure global shutter and global gain are off
      printf("Setting global Gains and Shutter to OFF\n");
      power = DC1394_OFF;
      dc1394_feature_set_power(camera, DC1394_FEATURE_SHUTTER, power);
      DC1394_ERR_RTN(err, "Failed to power off Shutter");
      power = DC1394_OFF;
      dc1394_feature_set_power(camera, DC1394_FEATURE_GAIN, power);
      DC1394_ERR_RTN(err, "Failed to power off Gain");
    } else if (std::string(argv[1]).compare("GLOBAL") == 0) {
      // Make sure it is on
      printf("Setting Global Gain to ON\n");
      power = DC1394_ON;
      dc1394_feature_set_power(camera, DC1394_FEATURE_GAIN, power);
      DC1394_ERR_RTN(err, "Failed to power on Gain");
      printf("Setting Global Gain to AUTO\n");
      mode = DC1394_FEATURE_MODE_AUTO;
      dc1394_feature_set_mode(camera, DC1394_FEATURE_GAIN, mode);
      DC1394_ERR_RTN(err, "Failed to set Gain mode");

      printf("Setting Global Shutter to ON\n");
      power = DC1394_ON;
      dc1394_feature_set_power(camera, DC1394_FEATURE_SHUTTER, power);
      DC1394_ERR_RTN(err, "Failed to power on Shutter");
      printf("Setting Global Shutter to AUTO\n");
      mode = DC1394_FEATURE_MODE_AUTO;
      dc1394_feature_set_mode(camera, DC1394_FEATURE_SHUTTER, mode);
      DC1394_ERR_RTN(err, "Failed to set Shutter mode");

      printf("Setting AutoExposure to ON\n");
      power = DC1394_ON;
      dc1394_feature_set_power(camera, DC1394_FEATURE_EXPOSURE, power);
      DC1394_ERR_RTN(err, "Failed to power on AutoExposure");
      printf("Setting AutoExposure to AUTO\n");
      mode = DC1394_FEATURE_MODE_AUTO;
      dc1394_feature_set_mode(camera, DC1394_FEATURE_EXPOSURE, mode);
      DC1394_ERR_RTN(err, "Failed to set AutoExposure mode");
    } else {
      print_help(argv);
    }
  } else {
    print_help(argv);
  }

  dc1394_camera_free(camera);
  dc1394_free(d);

  return 0;
}
