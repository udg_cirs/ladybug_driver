/*
 * Capture program prototype for the spherical 6-CCD Ladybug
 *     camera from Point Grey
 *
 * Original version written by Damien Douxchamps <ddouxchamps@users.sf.net>
 * Modified by Josep Bosch <jep250@gmail.com>
 *
 */

#include <dc1394/dc1394.h>
#include <iostream>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <vector>

// Camera parameters.

#define VIDEO_MODE DC1394_VIDEO_MODE_FORMAT7_7
#define OPERATION_MODE DC1394_OPERATION_MODE_1394B
#define ISO_SPEED DC1394_ISO_SPEED_800
#define BPP 8160
#define DMA_RING_BUFFER                                                        \
  10 // Put large to avoid losing frames. Stritly FIFO, is full new frames will
     // be dropped until there is space in buffer.
#define WIDTH 808
#define HEIGHT                                                                 \
  5000 // MAX 14784   No more fps smaller than 5000. Increase, decrease related
       // to fps

bool finished = false;

// Ctrl-c to stop
void my_handler(int s) {
  printf("Caught signal %d\n", s);
  // exit(1);
  finished = true;
}

int main(int argc, char **argv) {
  float total_time;
  char *directory;
  if (argc != 3) {
    std::cerr << "Usage :" << argv[0] << " <Directory> <Time>" << std::endl;
    std::cerr << "This will save all the frames (24 images per frame) in the "
                 "specified directory ( must be new and relative path) during "
                 "the Time (sec)"
              << std::endl;
    return 1;
  }

  directory = argv[1];        // Relative path to the directory
  total_time = atof(argv[2]); // Duration of the program.
  dc1394error_t err;
  dc1394camera_t *camera;
  dc1394video_frame_t *frame;
  char filename[256];
  int nframes = 0; // Captured frames.

  // Variables to retrieve directory path.
  long size;
  char *buf;
  char *basename;

  size = pathconf(".", _PC_PATH_MAX);
  if ((buf = (char *)malloc((size_t)size)) != NULL)
    basename = getcwd(buf, (size_t)size);

  // char directory;
  char directoryPath[200];
  // directory="test";

  sprintf(directoryPath, "%s/%s", basename, directory);

  int status;
  status = mkdir(directoryPath, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  if (status == -1) {
    printf("Cannot create directory or directory exists. Please choose another "
           "one or delete it.\n");
    exit(1);
  }

  // Handle ctrl-c
  struct sigaction sigIntHandler;

  sigIntHandler.sa_handler = my_handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;

  sigaction(SIGINT, &sigIntHandler, NULL);

  double start, prev; // start and  previous timestamps.
  double start_ladybug;
  double prev_ladybug;
  double total_diff = 0;       // Difference of time
  double max_diff = 0;         // Difference of time
  double max_diff_ladybug = 0; // Difference of time
  double diff = 0;
  // int total_frames_behind=0;
  int prev_ladybug_id;
  int lost_frames = 0;
  int total_lost_frames = 0;
  FILE *fd;

  dc1394_t *d;
  dc1394camera_list_t *list;

  d = dc1394_new();
  if (!d)
    return 1;
  err = dc1394_camera_enumerate(d, &list);
  DC1394_ERR_RTN(err, "Failed to enumerate cameras");

  if (list->num == 0) {
    dc1394_log_error("No cameras found");
    return 1;
  }

  camera = dc1394_camera_new(d, list->ids[0].guid);
  if (!camera) {
    dc1394_log_error("Failed to initialize camera with guid %llx",
                     list->ids[0].guid);
    return 1;
  }
  dc1394_camera_free_list(list);
  printf("Using camera \"%s %s\"\n", camera->vendor, camera->model);
  // setup video mode, etc...

  err = dc1394_video_set_operation_mode(camera, OPERATION_MODE);
  DC1394_ERR_RTN(err, "Could not set B mode");
  err = dc1394_video_set_iso_speed(camera, ISO_SPEED);
  DC1394_ERR_RTN(err, "Could not set 800Mbps speed");
  err = dc1394_video_set_mode(camera, VIDEO_MODE);
  DC1394_ERR_RTN(err, "Could not set video mode");

  err = dc1394_format7_set_roi(camera, VIDEO_MODE, DC1394_COLOR_CODING_MONO8,
                               BPP, 0, 0, WIDTH, HEIGHT);
  DC1394_ERR_RTN(err, "Could not set ROI");

  // setup capture
  err = dc1394_capture_setup(camera, DMA_RING_BUFFER,
                             DC1394_CAPTURE_FLAGS_DEFAULT);
  DC1394_ERR_RTN(err, "Could not setup capture");
  err = dc1394_video_set_transmission(camera, DC1394_ON);
  DC1394_ERR_RTN(err, "Could not start transmission");

  bool first_image = true;

  int cam, k = 0;
  unsigned int jpgadr, jpgsize, adr;
  double time_sec;
  while (total_diff < total_time && !finished) {
    // capture frame
    err = dc1394_capture_dequeue(camera, DC1394_CAPTURE_POLICY_WAIT, &frame);
    DC1394_ERR_RTN(err, "Could not dequeue a frame");

    /* Frames behind and frames id is useles!!
    int frames_behind=(int)frame->frames_behind;
    total_frames_behind+=frames_behind;
    printf("Frames behind: %d\n",frames_behind);

    int id=(int)frame->id;
    printf("Frame id: %d\n",id);
    */

    // Get timestamp of image.(TODO): Is embedded timestamp more accurate?
    time_sec = (double)frame->timestamp / 1000000;
    // printf("Timestamp: %f sec\n",time_sec);

    // Get Ladybug timestamp
    adr = 0x0018;
    unsigned int seconds = (((unsigned int)*(frame->image + adr)) << 24) +
                           (((unsigned int)*(frame->image + adr + 1)) << 16) +
                           (((unsigned int)*(frame->image + adr + 2)) << 8) +
                           (((unsigned int)*(frame->image + adr + 3)));

    adr = 0x001C;
    unsigned int microseconds =
        (((unsigned int)*(frame->image + adr)) << 24) +
        (((unsigned int)*(frame->image + adr + 1)) << 16) +
        (((unsigned int)*(frame->image + adr + 2)) << 8) +
        (((unsigned int)*(frame->image + adr + 3)));
    double timestamp_ladybug = seconds + microseconds / 1000000.0;

    adr = 0x0020; // Frame id
    int id_ladybug = (int)((((unsigned int)*(frame->image + adr)) << 24) +
                           (((unsigned int)*(frame->image + adr + 1)) << 16) +
                           (((unsigned int)*(frame->image + adr + 2)) << 8) +
                           (((unsigned int)*(frame->image + adr + 3))));
    // std::cout << "Ladybug id: " << id_ladybug << std::endl;

    /*
    // Save the image in separated files
    for (cam=0;cam<6;cam++) {
        for (k=0;k<4;k++) {
            adr=0x340+cam*32+k*8;
            jpgadr=(((unsigned int)*(frame->image+adr))<<24)+
                (((unsigned int)*(frame->image+adr+1))<<16)+
                (((unsigned int)*(frame->image+adr+2))<<8)+
                (((unsigned int)*(frame->image+adr+3)));
            adr+=4;
            jpgsize=(((unsigned int)*(frame->image+adr))<<24)+
                (((unsigned int)*(frame->image+adr+1))<<16)+
                (((unsigned int)*(frame->image+adr+2))<<8)+
                (((unsigned int)*(frame->image+adr+3)));

            if (jpgsize!=0) {
                sprintf(filename,"%s/%s%d_%s%05d_%d.jpg",directoryPath,"camera",cam,"frame",nframes,k);
                                    //printf("%s\n",filename);
                fd=fopen(filename,"w");
                fwrite((unsigned char *)(jpgadr+frame->image),jpgsize,1,fd);
                fclose(fd);
            }
        }
    }
    */

    // release frame
    err = dc1394_capture_enqueue(camera, frame);
    DC1394_ERR_RTN(err, "Could not enqueue a frame");
    fprintf(stderr, "%d\r", nframes);

    if (first_image) {
      start_ladybug = timestamp_ladybug;
      timestamp_ladybug = time_sec;
      first_image = false;
      start = time_sec;
    } else {
      timestamp_ladybug += start - start_ladybug;
      lost_frames = id_ladybug - prev_ladybug_id - 1;
      if (lost_frames != 0) {
        total_lost_frames += lost_frames;
        std::cout << "==============WARNING: " << lost_frames
                  << " FRAMES LOST==============" << std::endl;
        if (lost_frames < 0) {
          getchar();
        }
      }
      /* compute time delay */
      diff = time_sec - prev;
      double diff_ladybug = timestamp_ladybug - prev_ladybug;
      if (max_diff < diff) {
        max_diff = diff;
      }
      if (max_diff_ladybug < diff_ladybug) {
        max_diff_ladybug = diff_ladybug;
      }
      // printf("Time elapsed between frames: %f sec\n",diff);
      // printf("Time elapsed between frames ladybug: %f sec\n",diff_ladybug);
    }
    prev_ladybug_id = id_ladybug;
    // printf("Timestamp Ladybug: %f sec\n",timestamp_ladybug);

    // Save timestamp
    sprintf(filename, "%s/%s%05d.txt", directoryPath, "frame", nframes);
    fd = fopen(filename, "w");
    // fprintf(fd,"%lf",time_sec);
    fprintf(fd, "%lf", timestamp_ladybug);
    fclose(fd);

    total_diff = time_sec - start;
    prev = time_sec;
    prev_ladybug = timestamp_ladybug;

    std::cout << nframes << "\r" << std::flush;
    nframes++;
  }
  // stop capture
  err = dc1394_video_set_transmission(camera, DC1394_OFF);
  DC1394_ERR_RTN(err, "Could not stop transmission");
  err = dc1394_capture_stop(camera);
  DC1394_ERR_RTN(err, "Could not stop capture");
  dc1394_camera_free(camera);
  dc1394_free(d);
  // First frame doesn't count for computations of fps.
  std::cout << "Captured " << nframes << " frames in " << total_diff
            << "sec. Average: " << (nframes - 1) / total_diff
            << " fps. Max diff:" << max_diff << "sec."
            << " Max diff Ladybug: " << max_diff_ladybug << "sec." << std::endl;
  // std::cout << "Total frames behind: " << total_frames_behind << std::endl;
  std::cout << "Lost frames: " << total_lost_frames << std::endl;
  return 0;
}
