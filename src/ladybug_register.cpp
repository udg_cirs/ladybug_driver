/*
 * Program to get the calibration saved inside ladybug camera.
 *
 * Original version written by Josep Bosch <jep250@gmail.com>
 *
 */

#include <ctype.h> // isdigit()
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <dc1394/dc1394.h>

bool is_number(const std::string &s) {
  std::string::const_iterator it = s.begin();
  while (it != s.end() && std::isdigit(*it))
    ++it;
  return !s.empty() && it == s.end();
}

void print_help(char **argv) {
  std::cerr << "Usage :" << argv[0] << " <NewShutterValue>" << std::endl;
  std::cerr << "If given a new value will set this shutter value for all the "
               "cameras. If no value given it will show the current one. For "
               "auto instead of value write AUTO"
            << std::endl;
}

int main(int argc, char **argv) {

  if (argc > 2) {
    print_help(argv);
    return 1;
  }

  dc1394error_t err;
  dc1394_t *d;
  dc1394camera_list_t *list;
  dc1394camera_t *camera;
  dc1394feature_mode_t mode;

  d = dc1394_new();
  if (!d)
    return 1;
  err = dc1394_camera_enumerate(d, &list);
  DC1394_ERR_RTN(err, "Failed to enumerate cameras");

  if (list->num == 0) {
    dc1394_log_error("No cameras found");
    return 1;
  }

  camera = dc1394_camera_new(d, list->ids[0].guid);
  if (!camera) {
    dc1394_log_error("Failed to initialize camera with guid %llx",
                     list->ids[0].guid);
    return 1;
  }
  dc1394_camera_free_list(list);
  printf("Using camera \"%s %s\"\n", camera->vendor, camera->model);

  // Gets value of shutter.

  uint32_t val;
  float min_f, max_f, value_f;
  // uint32_t reg = 0x1000;
  uint64_t offset = 0x918;
  err = dc1394_get_control_register(camera, offset, &val);
  memcpy(&value_f, &val, sizeof(val));
  std::cout << value_f << std::endl;
  offset = 0x918 - 0x004;
  std::cout << offset << std::endl;
  std::cout << std::hex << offset << std::endl;
  err = dc1394_get_control_register(camera, offset, &val);
  memcpy(&value_f, &val, sizeof(val));
  std::cout << value_f << std::endl;
  offset = 0x918 - 0x008;
  err = dc1394_get_control_register(camera, offset, &val);
  memcpy(&value_f, &val, sizeof(val));
  std::cout << value_f << std::endl;

  dc1394_camera_free(camera);
  dc1394_free(d);

  return 0;
}
