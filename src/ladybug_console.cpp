/*
 * Program to get the calibration saved inside ladybug camera.
 *
 * Original version written by Josep Bosch <jep250@gmail.com>
 *
 */

#include <bitset>
#include <ctype.h> // isdigit()
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <dc1394/dc1394.h>

bool is_number(const std::string &s) {
  std::string::const_iterator it = s.begin();
  while (it != s.end() && std::isdigit(*it))
    ++it;
  return !s.empty() && it == s.end();
}

void print_help(char **argv) {
  std::cerr << "Usage :" << argv[0] << std::endl;
  std::cerr << "Shows all relevant parameters about ladybug3." << std::endl;
}

int get_hdr_values(dc1394camera_t *camera, uint32_t shutter_offset,
                   uint32_t *gain_value, uint32_t *shutter_value) {
  dc1394error_t err;
  // Get values of gain and shutter
  uint32_t gain_register_val;
  uint64_t gain_offset = shutter_offset + 0x04;
  err = dc1394_get_control_register(camera, gain_offset, &gain_register_val);
  DC1394_ERR_RTN(err, "Failed to get gain value");
  // Get 12 most right bits.
  *gain_value = gain_register_val & 0xFFF;
  uint32_t shutter_register_val;
  err = dc1394_get_control_register(camera, shutter_offset,
                                    &shutter_register_val);
  DC1394_ERR_RTN(err, "Failed to get shutter value");
  // Get 12 most right bits.
  *shutter_value = shutter_register_val & 0xFFF;
  return 0;
}

int main(int argc, char **argv) {

  if (argc > 1) {
    print_help(argv);
    return 1;
  }

  dc1394error_t err;
  dc1394_t *d;
  dc1394camera_list_t *list;
  dc1394camera_t *camera;
  dc1394feature_mode_t mode;
  dc1394switch_t power;

  d = dc1394_new();
  if (!d)
    return 1;
  err = dc1394_camera_enumerate(d, &list);
  DC1394_ERR_RTN(err, "Failed to enumerate cameras");

  if (list->num == 0) {
    dc1394_log_error("No cameras found");
    return 1;
  }

  camera = dc1394_camera_new(d, list->ids[0].guid);
  if (!camera) {
    dc1394_log_error("Failed to initialize camera with guid %llx",
                     list->ids[0].guid);
    return 1;
  }
  dc1394_camera_free_list(list);
  printf("Using camera \"%s %s\"\n", camera->vendor, camera->model);

  if (argc == 1) {
    // Retrieve info of the mask
    //============CAMERA0=====================
    uint32_t register_val;
    uint64_t mask_offset = 0x1E90;
    err = dc1394_get_control_register(camera, mask_offset, &register_val);
    DC1394_ERR_RTN(err, "Failed to get mask value");
    uint32_t mask_value = register_val & 0x3F; // get last 6 bits
    std::cout << "============AUTOEXPOSURE MASK VALUES============"
              << std::endl;
    std::cout << "MASK : Current: \t" << (std::bitset<6>)mask_value
              << std::endl;
    // Retrieve info for each one of the cameras.
    std::cout << "============INDIVIDUAL (RELATIVE) VALUES============"
              << std::endl;
    //============CAMERA0=====================
    uint32_t gain_register_val;
    uint64_t gain_offset = 0x1B00;
    err = dc1394_get_control_register(camera, gain_offset, &gain_register_val);
    DC1394_ERR_RTN(err, "Failed to get gain value");
    // Get 12 most right bits.
    uint32_t gain_value;
    gain_value = gain_register_val & 0xFFF;
    uint32_t shutter_register_val;
    uint64_t shutter_offset = 0x1B04;
    err = dc1394_get_control_register(camera, shutter_offset,
                                      &shutter_register_val);
    DC1394_ERR_RTN(err, "Failed to get shutter value");
    // Get 12 most right bits.
    uint32_t shutter_value;
    shutter_value = shutter_register_val & 0xFFF;
    uint32_t exposure_register_val;
    uint64_t exposure_offset = 0x1B08;
    err = dc1394_get_control_register(camera, exposure_offset,
                                      &exposure_register_val);
    DC1394_ERR_RTN(err, "Failed to get exposure value");
    // Get 12 most right bits.
    uint32_t exposure_value;
    exposure_value = exposure_register_val & 0xFFF;
    std::cout << "CAMERA 0 \t Gain:\t" << std::dec << gain_value
              << "\t Shutter: \t" << shutter_value << "\t Exposure: \t"
              << exposure_value << std::endl;

    //============CAMERA1=====================
    gain_offset = 0x1B20;
    err = dc1394_get_control_register(camera, gain_offset, &gain_register_val);
    DC1394_ERR_RTN(err, "Failed to get gain value");
    // Get 12 most right bits.
    gain_value = gain_register_val & 0xFFF;
    shutter_offset = 0x1B24;
    err = dc1394_get_control_register(camera, shutter_offset,
                                      &shutter_register_val);
    DC1394_ERR_RTN(err, "Failed to get shutter value");
    // Get 12 most right bits.
    shutter_value = shutter_register_val & 0xFFF;
    exposure_offset = 0x1B28;
    err = dc1394_get_control_register(camera, exposure_offset,
                                      &exposure_register_val);
    DC1394_ERR_RTN(err, "Failed to get exposure value");
    // Get 12 most right bits.
    exposure_value = exposure_register_val & 0xFFF;
    std::cout << "CAMERA 1 \t Gain:\t" << std::dec << gain_value
              << "\t Shutter: \t" << shutter_value << "\t Exposure: \t"
              << exposure_value << std::endl;

    //============CAMERA2=====================
    gain_offset = 0x1B40;
    err = dc1394_get_control_register(camera, gain_offset, &gain_register_val);
    DC1394_ERR_RTN(err, "Failed to get gain value");
    // Get 12 most right bits.
    gain_value = gain_register_val & 0xFFF;
    shutter_offset = 0x1B44;
    err = dc1394_get_control_register(camera, shutter_offset,
                                      &shutter_register_val);
    DC1394_ERR_RTN(err, "Failed to get shutter value");
    // Get 12 most right bits.
    shutter_value = shutter_register_val & 0xFFF;
    exposure_offset = 0x1B48;
    err = dc1394_get_control_register(camera, exposure_offset,
                                      &exposure_register_val);
    DC1394_ERR_RTN(err, "Failed to get exposure value");
    // Get 12 most right bits.
    exposure_value = exposure_register_val & 0xFFF;
    std::cout << "CAMERA 2 \t Gain:\t" << std::dec << gain_value
              << "\t Shutter: \t" << shutter_value << "\t Exposure: \t"
              << exposure_value << std::endl;
    //============CAMERA3=====================
    gain_offset = 0x1B60;
    err = dc1394_get_control_register(camera, gain_offset, &gain_register_val);
    DC1394_ERR_RTN(err, "Failed to get gain value");
    // Get 12 most right bits.
    gain_value = gain_register_val & 0xFFF;
    shutter_offset = 0x1B64;
    err = dc1394_get_control_register(camera, shutter_offset,
                                      &shutter_register_val);
    DC1394_ERR_RTN(err, "Failed to get shutter value");
    // Get 12 most right bits.
    shutter_value = shutter_register_val & 0xFFF;
    exposure_offset = 0x1B68;
    err = dc1394_get_control_register(camera, exposure_offset,
                                      &exposure_register_val);
    DC1394_ERR_RTN(err, "Failed to get exposure value");
    // Get 12 most right bits.
    exposure_value = exposure_register_val & 0xFFF;
    std::cout << "CAMERA 3 \t Gain:\t" << std::dec << gain_value
              << "\t Shutter: \t" << shutter_value << "\t Exposure: \t"
              << exposure_value << std::endl;
    //============CAMERA4=====================
    gain_offset = 0x1B80;
    err = dc1394_get_control_register(camera, gain_offset, &gain_register_val);
    DC1394_ERR_RTN(err, "Failed to get gain value");
    // Get 12 most right bits.
    gain_value = gain_register_val & 0xFFF;
    shutter_offset = 0x1B84;
    err = dc1394_get_control_register(camera, shutter_offset,
                                      &shutter_register_val);
    DC1394_ERR_RTN(err, "Failed to get shutter value");
    // Get 12 most right bits.
    shutter_value = shutter_register_val & 0xFFF;
    exposure_offset = 0x1B88;
    err = dc1394_get_control_register(camera, exposure_offset,
                                      &exposure_register_val);
    DC1394_ERR_RTN(err, "Failed to get exposure value");
    // Get 12 most right bits.
    exposure_value = exposure_register_val & 0xFFF;
    std::cout << "CAMERA 4 \t Gain:\t" << std::dec << gain_value
              << "\t Shutter: \t" << shutter_value << "\t Exposure: \t"
              << exposure_value << std::endl;
    //============CAMERA5=====================
    gain_offset = 0x1BA0;
    err = dc1394_get_control_register(camera, gain_offset, &gain_register_val);
    DC1394_ERR_RTN(err, "Failed to get gain value");
    // Get 12 most right bits.
    gain_value = gain_register_val & 0xFFF;
    shutter_offset = 0x1BA4;
    err = dc1394_get_control_register(camera, shutter_offset,
                                      &shutter_register_val);
    DC1394_ERR_RTN(err, "Failed to get shutter value");
    // Get 12 most right bits.
    shutter_value = shutter_register_val & 0xFFF;
    exposure_offset = 0x1BA8;
    err = dc1394_get_control_register(camera, exposure_offset,
                                      &exposure_register_val);
    DC1394_ERR_RTN(err, "Failed to get exposure value");
    // Get 12 most right bits.
    exposure_value = exposure_register_val & 0xFFF;
    std::cout << "CAMERA 5 \t Gain:\t" << std::dec << gain_value
              << "\t Shutter: \t" << shutter_value << "\t Exposure: \t"
              << exposure_value << std::endl;

    // Retrieve info about absolute values.
    uint32_t abs_shutter_val;
    float min_f, max_f, value_f;
    uint64_t abs_shutter_offset = 0x918;
    err = dc1394_get_control_register(camera, abs_shutter_offset,
                                      &abs_shutter_val);
    DC1394_ERR_RTN(err, "Failed to get shutter absolute values");
    memcpy(&value_f, &abs_shutter_val, sizeof(abs_shutter_val));
    err = dc1394_get_control_register(camera, abs_shutter_offset - 0x04,
                                      &abs_shutter_val);
    DC1394_ERR_RTN(err, "Failed to get shutter absolute values");
    memcpy(&max_f, &abs_shutter_val, sizeof(abs_shutter_val));
    err = dc1394_get_control_register(camera, abs_shutter_offset - 0x08,
                                      &abs_shutter_val);
    DC1394_ERR_RTN(err, "Failed to get shutter absolute values");
    memcpy(&min_f, &abs_shutter_val, sizeof(abs_shutter_val));

    std::cout << "============ABSOLUTE VALUES============" << std::endl;
    std::cout << "SHUTTER [Sec]: Current: \t" << value_f << "\t Max: \t"
              << max_f << "\t Min: \t" << min_f << std::endl;

    uint32_t abs_gain_val;
    uint64_t abs_gain_offset = 0x928;
    err = dc1394_get_control_register(camera, abs_gain_offset, &abs_gain_val);
    DC1394_ERR_RTN(err, "Failed to get gain absolute values");
    memcpy(&value_f, &abs_gain_val, sizeof(abs_shutter_val));
    err = dc1394_get_control_register(camera, abs_gain_offset - 0x04,
                                      &abs_gain_val);
    DC1394_ERR_RTN(err, "Failed to get gain absolute values");
    memcpy(&max_f, &abs_gain_val, sizeof(abs_shutter_val));
    err = dc1394_get_control_register(camera, abs_gain_offset - 0x08,
                                      &abs_gain_val);
    DC1394_ERR_RTN(err, "Failed to get gain absolute values");
    memcpy(&min_f, &abs_gain_val, sizeof(abs_shutter_val));

    std::cout << "GAIN [dB]: Current: \t" << value_f << "\t Max: \t" << max_f
              << "\t Min: \t" << min_f << std::endl;

    std::cout << "============JPEG COMPRESSION============" << std::endl;
    // Retrieve info about jpeg compression.
    uint64_t jpeg_offset = 0x1E80;
    err = dc1394_get_control_register(camera, jpeg_offset, &register_val);
    DC1394_ERR_RTN(err, "Failed to get jpeg compression rate");
    uint32_t mode_value = register_val & 0x2000000;
    if (mode_value == 0) {
      std::cout << "Jpeg: \tON" << std::endl;
    } else if (mode_value == 1) {
      std::cout << "Jpeg: \tOFF" << std::endl;
    }
    mode_value = register_val & 0x1000000;
    if (mode_value == 0) {
      std::cout << "Jpeg mode: \tMANUAL" << std::endl;
    } else if (mode_value == 0x1000000) {
      std::cout << "Jpeg mode: \tAUTO" << std::endl;
    }
    uint32_t rate_value = register_val & 0xFFF;
    std::cout << "Jpeg compression rate: \t" << rate_value << std::endl;

    // Retrieve info about absolute values.
    uint32_t abs_fps_val;
    uint64_t abs_fps_offset = 0x968;
    err = dc1394_get_control_register(camera, abs_fps_offset, &abs_fps_val);
    DC1394_ERR_RTN(err, "Failed to get fps absolute values");
    memcpy(&value_f, &abs_fps_val, sizeof(abs_fps_val));
    err = dc1394_get_control_register(camera, abs_fps_offset - 0x04,
                                      &abs_fps_val);
    DC1394_ERR_RTN(err, "Failed to get fps absolute values");
    memcpy(&max_f, &abs_fps_val, sizeof(abs_fps_val));
    err = dc1394_get_control_register(camera, abs_fps_offset - 0x08,
                                      &abs_fps_val);
    DC1394_ERR_RTN(err, "Failed to get fps absolute values");
    memcpy(&min_f, &abs_fps_val, sizeof(abs_fps_val));

    std::cout << "============FPS============" << std::endl;
    std::cout << "Fps: Current: \t" << value_f << "\t Max: \t" << max_f
              << "\t Min: \t" << min_f << std::endl;

    // Retrieve info about HDR
    uint64_t hdr_offset = 0x1800;
    std::string hdr_enabled = "OFF";
    err = dc1394_get_control_register(camera, hdr_offset, &register_val);
    DC1394_ERR_RTN(err, "Failed to get hdr value");
    uint32_t hdr_value = register_val & 0x2000000; // get 6th bit
    if (hdr_value == 0x2000000) {
      hdr_enabled = "ON";
    }
    std::cout << "===============HDR===============" << std::endl;
    std::cout << "HDR : Enabled: \t" << hdr_enabled << std::endl;

    // Get HDR 0 values.
    shutter_offset = 0x1820;
    get_hdr_values(camera, shutter_offset, &gain_value, &shutter_value);
    std::cout << "HDR setting 0 \t Gain:\t" << std::dec << gain_value
              << "\t Shutter: \t" << shutter_value << std::endl;
    shutter_offset = 0x1840;
    get_hdr_values(camera, shutter_offset, &gain_value, &shutter_value);
    std::cout << "HDR setting 1 \t Gain:\t" << std::dec << gain_value
              << "\t Shutter: \t" << shutter_value << std::endl;
    shutter_offset = 0x1860;
    get_hdr_values(camera, shutter_offset, &gain_value, &shutter_value);
    std::cout << "HDR setting 2 \t Gain:\t" << std::dec << gain_value
              << "\t Shutter: \t" << shutter_value << std::endl;
    shutter_offset = 0x1880;
    get_hdr_values(camera, shutter_offset, &gain_value, &shutter_value);
    std::cout << "HDR setting 3 \t Gain:\t" << std::dec << gain_value
              << "\t Shutter: \t" << shutter_value << std::endl;

  } else {
    print_help(argv);
  }

  dc1394_camera_free(camera);
  dc1394_free(d);

  return 0;
}
