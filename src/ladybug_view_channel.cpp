/*
 * Capture program prototype for the spherical 6-CCD Ladybug
 *     camera from Point Grey
 *
 * Original version written by Damien Douxchamps <ddouxchamps@users.sf.net>
 * Modified by Josep Bosch <jep250@gmail.com>
 *
 */

#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>

#include <dc1394/dc1394.h>

// Camera parameters.

#define VIDEO_MODE DC1394_VIDEO_MODE_FORMAT7_7
#define OPERATION_MODE DC1394_OPERATION_MODE_1394B
#define ISO_SPEED DC1394_ISO_SPEED_800
#define BPP 8160
#define DMA_RING_BUFFER 4
#define WIDTH 808
#define HEIGHT                                                                 \
  5000 // MAX 14784   No more fps smaller than 5000. Increase, decrease related
       // to fps

// Ctrl-c to stop
void my_handler(int s) {
  printf("Caught signal %d\n", s);
  exit(1);
}

int main(int argc, char **argv) {
  int ncam, camstart, camend;
  std::string channel;
  if (argc != 3) {
    std::cerr << "Usage:" << argv[0] << " <Camera> <Colour>" << std::endl;
    std::cerr << "This will show in a new window the latest jpg image.Camera "
                 "0-5. To see al cameras: 6. If want colour set to 'All'. If "
                 "set to 'Red', 'Green' or 'Blue' show only that channel. In "
                 "case of green only one channel is shown."
              << std::endl;
    return 1;
  }

  ncam = atoi(argv[1]);
  channel = argv[2];

  int start, end;

  if (channel.compare("All") == 0) {
    start = 0;
    end = 4;
  } else if (channel.compare("Red") == 0) {
    start = 0;
    end = 1;
  } else if (channel.compare("Green") == 0) {
    start = 1;
    end = 2;
  } else if (channel.compare("Blue") == 0) {
    start = 3;
    end = 4;
  } else {
    std::cerr << "Unknown channel " << std::endl;
    return 1;
  }

  if (ncam < 6) {
    camstart = ncam;
    camend = ncam + 1;
  } else {
    camstart = 0;
    camend = 6;
  }

  dc1394error_t err;
  dc1394camera_t *camera;
  dc1394video_frame_t *frame;
  char filename[256];
  int nframes = 0; // Captured frames.

  // Handle ctrl-c
  struct sigaction sigIntHandler;

  sigIntHandler.sa_handler = my_handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;

  sigaction(SIGINT, &sigIntHandler, NULL);

  dc1394_t *d;
  dc1394camera_list_t *list;

  d = dc1394_new();
  if (!d)
    return 1;
  err = dc1394_camera_enumerate(d, &list);
  DC1394_ERR_RTN(err, "Failed to enumerate cameras");

  if (list->num == 0) {
    dc1394_log_error("No cameras found");
    return 1;
  }

  camera = dc1394_camera_new(d, list->ids[0].guid);
  if (!camera) {
    dc1394_log_error("Failed to initialize camera with guid %llx",
                     list->ids[0].guid);
    return 1;
  }
  dc1394_camera_free_list(list);
  printf("Using camera \"%s %s\"\n", camera->vendor, camera->model);
  // setup video mode, etc...

  err = dc1394_video_set_operation_mode(camera, OPERATION_MODE);
  DC1394_ERR_RTN(err, "Could not set B mode");
  err = dc1394_video_set_iso_speed(camera, ISO_SPEED);
  DC1394_ERR_RTN(err, "Could not set 800Mbps speed");
  err = dc1394_video_set_mode(camera, VIDEO_MODE);
  DC1394_ERR_RTN(err, "Could not set video mode");

  err = dc1394_format7_set_roi(camera, VIDEO_MODE, DC1394_COLOR_CODING_MONO8,
                               BPP, 0, 0, WIDTH, HEIGHT);
  DC1394_ERR_RTN(err, "Could not set ROI");

  // setup capture
  err = dc1394_capture_setup(camera, DMA_RING_BUFFER,
                             DC1394_CAPTURE_FLAGS_DEFAULT);
  DC1394_ERR_RTN(err, "Could not setup capture");
  err = dc1394_video_set_transmission(camera, DC1394_ON);
  DC1394_ERR_RTN(err, "Could not start transmission");

  // Ready

  cv::namedWindow("Display window",
                  CV_WINDOW_AUTOSIZE); // Create a window for display.

  int cam, k = 0;
  unsigned int jpgadr, jpgsize, adr;

  // Allocate space for 6x4 images, even that we know in most cases we will not
  // use them.
  cv::Mat imgMat[24] = {cv::Mat::zeros(616, 808, CV_8U)};       // height, width
  cv::Mat imgDisplay = cv::Mat::zeros(308, 404, CV_8U);         // height, width
  cv::Mat imgDisplayColour = cv::Mat::zeros(308, 404, CV_8UC3); // height, width
  cv::Mat imgDisplaySix = cv::Mat::zeros(
      imgDisplay.rows * 2, imgDisplay.cols * 3, CV_8U); // height, width
  cv::Mat imgDisplaySixColour = cv::Mat::zeros(
      imgDisplay.rows * 2, imgDisplay.cols * 3, CV_8UC3); // height, width
  while (1) {
    // capture frame
    err = dc1394_capture_dequeue(camera, DC1394_CAPTURE_POLICY_WAIT, &frame);
    DC1394_ERR_RTN(err, "Could not dequeue a frame");

    // Save the image in separated files
    for (cam = camstart; cam < camend; cam++) {
      for (k = start; k < end; k++) {
        adr = 0x340 + cam * 32 + k * 8;
        jpgadr = (((unsigned int)*(frame->image + adr)) << 24) +
                 (((unsigned int)*(frame->image + adr + 1)) << 16) +
                 (((unsigned int)*(frame->image + adr + 2)) << 8) +
                 (((unsigned int)*(frame->image + adr + 3)));
        adr += 4;
        jpgsize = (((unsigned int)*(frame->image + adr)) << 24) +
                  (((unsigned int)*(frame->image + adr + 1)) << 16) +
                  (((unsigned int)*(frame->image + adr + 2)) << 8) +
                  (((unsigned int)*(frame->image + adr + 3)));

        if (jpgsize != 0) {
          std::vector<unsigned char> buffer(jpgsize);
          memcpy(&(buffer[0]), (unsigned char *)frame->image + jpgadr, jpgsize);
          imgMat[4 * cam + k] = cv::imdecode(buffer, CV_LOAD_IMAGE_GRAYSCALE);
          if (!imgMat[4 * cam + k].data) // Check for invalid input
          {
            std::cout << "Could not decode the image" << std::endl;
          }
        }
      }
      if (ncam != 6) {
        // Show the image on a window now! :D
        if (channel.compare("Red") == 0) {
          cv::resize(imgMat[4 * cam + 0], imgDisplay, imgDisplay.size(), 0, 0,
                     cv::INTER_LINEAR);
          cv::imshow("Display window", imgDisplay);
        } else if (channel.compare("Green") == 0) {
          cv::resize(imgMat[4 * cam + 1], imgDisplay, imgDisplay.size(), 0, 0,
                     cv::INTER_LINEAR);
          cv::imshow("Display window", imgDisplay);
        } else if (channel.compare("Blue") == 0) {
          cv::resize(imgMat[4 * cam + 3], imgDisplay, imgDisplay.size(), 0, 0,
                     cv::INTER_LINEAR);
          cv::imshow("Display window", imgDisplay);
        } else if (channel.compare("All") == 0) {
          // Reconstruct the original image from the four channels! RGGB
          cv::Mat Reconstructed = cv::Mat::zeros(1232, 1616, CV_8U);
          int x, y;
          for (y = 0; y < 1232; y++) {
            uint8_t *row = Reconstructed.ptr<uint8_t>(y);
            if (y % 2 == 0) {
              uint8_t *i0 = imgMat[4 * cam + 0].ptr<uint8_t>(y / 2);
              uint8_t *i1 = imgMat[4 * cam + 1].ptr<uint8_t>(y / 2);

              for (x = 0; x < 1616;) {
                // R
                row[x] = i0[x / 2];
                x++;

                // G1
                row[x] = i1[x / 2];
                x++;
              }
            } else {
              uint8_t *i2 = imgMat[4 * cam + 2].ptr<uint8_t>(y / 2);
              uint8_t *i3 = imgMat[4 * cam + 3].ptr<uint8_t>(y / 2);

              for (x = 0; x < 1616;) {
                // G2
                row[x] = i2[x / 2];
                x++;

                // B
                row[x] = i3[x / 2];
                x++;
              }
            }
          }
          // Debayer
          cv::Mat ReconstructedColor;
          cv::cvtColor(Reconstructed, ReconstructedColor, CV_BayerBG2BGR);

          cv::resize(ReconstructedColor, imgDisplayColour, imgDisplay.size(), 0,
                     0, cv::INTER_LINEAR);
          // Display
          cv::imshow("Display window", imgDisplayColour);
        }
        cv::waitKey(30);
      }
      // Want to see all 6 cameras at same time
      else {
        int r = cam % 3;
        int j = cam / 3;
        if (channel.compare("Red") == 0) {
          cv::resize(imgMat[4 * cam + 0], imgDisplay, imgDisplay.size(), 0, 0,
                     cv::INTER_LINEAR);
          imgDisplay.copyTo(
              imgDisplaySix(cv::Rect(imgDisplay.cols * r, imgDisplay.rows * j,
                                     imgDisplay.cols, imgDisplay.rows)));
        } else if (channel.compare("Green") == 0) {
          cv::resize(imgMat[4 * cam + 1], imgDisplay, imgDisplay.size(), 0, 0,
                     cv::INTER_LINEAR);
          imgDisplay.copyTo(
              imgDisplaySix(cv::Rect(imgDisplay.cols * r, imgDisplay.rows * j,
                                     imgDisplay.cols, imgDisplay.rows)));
        } else if (channel.compare("Blue") == 0) {
          cv::resize(imgMat[4 * cam + 3], imgDisplay, imgDisplay.size(), 0, 0,
                     cv::INTER_LINEAR);
          imgDisplay.copyTo(
              imgDisplaySix(cv::Rect(imgDisplay.cols * r, imgDisplay.rows * j,
                                     imgDisplay.cols, imgDisplay.rows)));
        } else if (channel.compare("All") == 0) {
          // Reconstruct the original image from the four channels! RGGB
          cv::Mat Reconstructed = cv::Mat::zeros(1232, 1616, CV_8U);
          int x, y;
          for (y = 0; y < 1232; y++) {
            uint8_t *row = Reconstructed.ptr<uint8_t>(y);
            if (y % 2 == 0) {
              uint8_t *i0 = imgMat[4 * cam + 0].ptr<uint8_t>(y / 2);
              uint8_t *i1 = imgMat[4 * cam + 1].ptr<uint8_t>(y / 2);

              for (x = 0; x < 1616;) {
                // R
                row[x] = i0[x / 2];
                x++;

                // G1
                row[x] = i1[x / 2];
                x++;
              }
            } else {
              uint8_t *i2 = imgMat[4 * cam + 2].ptr<uint8_t>(y / 2);
              uint8_t *i3 = imgMat[4 * cam + 3].ptr<uint8_t>(y / 2);

              for (x = 0; x < 1616;) {
                // G2
                row[x] = i2[x / 2];
                x++;

                // B
                row[x] = i3[x / 2];
                x++;
              }
            }
          }
          // Debayer
          cv::Mat ReconstructedColor;
          cv::cvtColor(Reconstructed, ReconstructedColor, CV_BayerBG2BGR);
          cv::resize(ReconstructedColor, imgDisplayColour, imgDisplay.size(), 0,
                     0, cv::INTER_LINEAR);
          int r = cam % 3;
          int j = cam / 3;
          imgDisplayColour.copyTo(imgDisplaySixColour(
              cv::Rect(imgDisplay.cols * r, imgDisplay.rows * j,
                       imgDisplay.cols, imgDisplay.rows)));
        }
        if (cam == 5) {
          // If it's last camera show image on screen!
          if (channel.compare("All") == 0) {
            cv::imshow("Display window", imgDisplaySixColour);
          } else {
            cv::imshow("Display window", imgDisplaySix);
            cv::waitKey(30);
          }
          cv::waitKey(30);
        }
      }
    }
    // release frame
    err = dc1394_capture_enqueue(camera, frame);
    DC1394_ERR_RTN(err, "Could not enqueue a frame");
    fprintf(stderr, "%d\r", nframes);
    nframes++;
  }
  // stop capture
  err = dc1394_video_set_transmission(camera, DC1394_OFF);
  DC1394_ERR_RTN(err, "Could not stop transmission");
  err = dc1394_capture_stop(camera);
  DC1394_ERR_RTN(err, "Could not stop capture");
  dc1394_camera_free(camera);
  dc1394_free(d);
  std::cout << "Captued " << nframes << " frames." << std::endl;
  return 0;
}
