/*
 * Capture program prototype for the spherical 6-CCD Ladybug
 *     camera from Point Grey
 *
 * Original version written by Damien Douxchamps <ddouxchamps@users.sf.net>
 * Modified by Josep Bosch <jep250@gmail.com>
 *
 */

#include <iostream>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include <dc1394/dc1394.h>

// Camera parameters.

#define VIDEO_MODE DC1394_VIDEO_MODE_FORMAT7_1
#define OPERATION_MODE DC1394_OPERATION_MODE_1394B
#define ISO_SPEED DC1394_ISO_SPEED_800
#define BPP 8160
#define DMA_RING_BUFFER 4
#define WIDTH 808
#define HEIGHT 616

bool finished = false;

// Ctrl-c to stop
void my_handler(int s) {
  printf("Caught signal %d\n", s);
  finished = true;
}

int main(int argc, char **argv) {
  float total_time;
  char *directory;
  if (argc != 3) {
    std::cerr << "Usage :" << argv[0] << " <Directory> <Time>" << std::endl;
    std::cerr << "This will save all the frames (6x4 images per frame) in the "
                 "specified directory ( must be new and relative path) during "
                 "the Time (sec)"
              << std::endl;
    return 1;
  }

  directory = argv[1];        // Relative path to the directory
  total_time = atof(argv[2]); // Duration of the program.
  dc1394error_t err;
  dc1394camera_t *camera;
  dc1394video_frame_t *frame;
  char filename[256];
  int nframes = 0; // Captured frames.

  // Variables to retrieve directory path.
  long size;
  char *buf;
  char *basename;

  size = pathconf(".", _PC_PATH_MAX);
  if ((buf = (char *)malloc((size_t)size)) != NULL)
    basename = getcwd(buf, (size_t)size);

  // char directory;
  char directoryPath[200];
  // directory="test";

  sprintf(directoryPath, "%s/%s", basename, directory);

  int status;
  status = mkdir(directoryPath, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  if (status == -1) {
    printf("Cannot create directory or directory exists. Please choose another "
           "one or delete it.\n");
    exit(1);
  }

  // Handle ctrl-c
  struct sigaction sigIntHandler;

  sigIntHandler.sa_handler = my_handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;

  sigaction(SIGINT, &sigIntHandler, NULL);

  double start, prev;    // start and  previous timestamps.
  double total_diff = 0; // Difference of time
  double diff = 0;
  FILE *fd;

  dc1394_t *d;
  dc1394camera_list_t *list;

  d = dc1394_new();
  if (!d)
    return 1;
  err = dc1394_camera_enumerate(d, &list);
  DC1394_ERR_RTN(err, "Failed to enumerate cameras");

  if (list->num == 0) {
    dc1394_log_error("No cameras found");
    return 1;
  }

  camera = dc1394_camera_new(d, list->ids[0].guid);
  if (!camera) {
    dc1394_log_error("Failed to initialize camera with guid %llx",
                     list->ids[0].guid);
    return 1;
  }
  dc1394_camera_free_list(list);
  printf("Using camera \"%s %s\"\n", camera->vendor, camera->model);
  // setup video mode, etc...

  err = dc1394_video_set_operation_mode(camera, OPERATION_MODE);
  DC1394_ERR_RTN(err, "Could not set B mode");
  err = dc1394_video_set_iso_speed(camera, ISO_SPEED);
  DC1394_ERR_RTN(err, "Could not set 800Mbps speed");
  err = dc1394_video_set_mode(camera, VIDEO_MODE);
  DC1394_ERR_RTN(err, "Could not set video mode");

  err = dc1394_format7_set_roi(camera, VIDEO_MODE, DC1394_COLOR_CODING_MONO8,
                               BPP, 0, 0, WIDTH, HEIGHT * 6 * 4);
  DC1394_ERR_RTN(err, "Could not set ROI");

  // setup capture
  err = dc1394_capture_setup(camera, DMA_RING_BUFFER,
                             DC1394_CAPTURE_FLAGS_DEFAULT);
  DC1394_ERR_RTN(err, "Could not setup capture");
  err = dc1394_video_set_transmission(camera, DC1394_ON);
  DC1394_ERR_RTN(err, "Could not start transmission");

  bool first_image = true;

  // Header of all images.
  char str[256];
  int n;
  n = sprintf(str, "%s%d%s%d%s%d%s", "P5 ", WIDTH, " ", HEIGHT, " ", 255, " ");
  double time_sec;
  while (total_diff < total_time && !finished) {
    // capture frame
    err = dc1394_capture_dequeue(camera, DC1394_CAPTURE_POLICY_WAIT, &frame);
    DC1394_ERR_RTN(err, "Could not dequeue a frame");

    // Get timestamp of image.(TODO): Is embedded timestamp more accurate?
    time_sec = (double)frame->timestamp / 1000000;

    // Save timestamp
    sprintf(filename, "%s/%s%05d.txt", directoryPath, "frame", nframes);
    fd = fopen(filename, "w");
    fprintf(fd, "%lf", time_sec);
    fclose(fd);

    // Save the image in separated files
    int adr, cam, k;
    int width = WIDTH;
    int height = HEIGHT;

    for (cam = 0; cam < 6; cam++) {
      for (k = 0; k < 4; k++) {
        adr = 0x000 + cam * width * height * 4 + k * width * height;
        int image_size = height * width;
        char str[256];
        int n;
        n = sprintf(str, "%s%d%s%d%s%d%s", "P5 ", width, " ", height, " ", 255,
                    " ");
        sprintf(filename, "%s/%s%d_%s%05d_%d.pgm", directoryPath, "camera", cam,
                "frame", nframes, k);
        fd = fopen(filename, "w");
        // Write header
        fwrite(str, 1, n, fd);
        fwrite((unsigned char *)(frame->image + adr), image_size, 1, fd);
        fclose(fd);
      }
    }
    // release frame
    err = dc1394_capture_enqueue(camera, frame);
    DC1394_ERR_RTN(err, "Could not enqueue a frame");
    fprintf(stderr, "%d\r", nframes);
    nframes++;

    if (first_image) {
      first_image = false;
      start = time_sec;
    } else {
      /* compute time delay */
      diff = time_sec - prev;
      printf("Time elapsed between frames: %f sec\n", diff);
    }
    total_diff = time_sec - start;
    prev = time_sec;
  }
  // stop capture
  err = dc1394_video_set_transmission(camera, DC1394_OFF);
  DC1394_ERR_RTN(err, "Could not stop transmission");
  err = dc1394_capture_stop(camera);
  DC1394_ERR_RTN(err, "Could not stop capture");
  dc1394_camera_free(camera);
  dc1394_free(d);
  std::cout << "Captued " << nframes << " frames in " << total_diff
            << "sec. Average: " << (nframes - 1) / total_diff << " fps."
            << std::endl;
  return 0;
}
