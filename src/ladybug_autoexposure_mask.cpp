/*
 * Program to get the calibration saved inside ladybug camera.
 *
 * Original version written by Josep Bosch <jep250@gmail.com>
 *
 */

#include <bitset>
#include <ctype.h> // isdigit()
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <dc1394/dc1394.h>

bool is_valid(const std::string &s) {
  if (s.size() != 1) {
    return false;
  }
  if (s.compare("0") == 0 or s.compare("1") == 0) {
    return true;
  } else {
    return false;
  }
}

void print_help(char **argv) {
  std::cerr << "Usage :" << argv[0]
            << " <Mask0> <Mask1> <Mask2> <Mask3> <Mask4> <Mask5>" << std::endl;
  std::cerr << "Mask used to set autoexposure when in Global aut mode."
            << std::endl;
}

int main(int argc, char **argv) {

  if (argc != 1 and argc != 7) {
    print_help(argv);
    return 1;
  }

  dc1394error_t err;
  dc1394_t *d;
  dc1394camera_list_t *list;
  dc1394camera_t *camera;
  dc1394feature_mode_t mode;
  dc1394switch_t power;

  d = dc1394_new();
  if (!d)
    return 1;
  err = dc1394_camera_enumerate(d, &list);
  DC1394_ERR_RTN(err, "Failed to enumerate cameras");

  if (list->num == 0) {
    dc1394_log_error("No cameras found");
    return 1;
  }

  camera = dc1394_camera_new(d, list->ids[0].guid);
  if (!camera) {
    dc1394_log_error("Failed to initialize camera with guid %llx",
                     list->ids[0].guid);
    return 1;
  }
  dc1394_camera_free_list(list);
  printf("Using camera \"%s %s\"\n", camera->vendor, camera->model);

  if (argc == 1) {
    // Retrieve info of the mask
    uint32_t register_val;
    uint64_t mask_offset = 0x1E90;
    err = dc1394_get_control_register(camera, mask_offset, &register_val);
    DC1394_ERR_RTN(err, "Failed to get mask value");
    uint32_t mask_value = register_val & 0x3F; // get last 6 bits
    std::cout << "============AUTOEXPOSURE MASK VALUES============"
              << std::endl;
    std::cout << "MASK : Current: \t" << (std::bitset<6>)mask_value
              << std::endl;
  } else {
    if (is_valid(argv[1]) and is_valid(argv[2]) and is_valid(argv[3]) and
        is_valid(argv[4]) and is_valid(argv[5]) and is_valid(argv[6])) {
      uint32_t register_val;
      uint64_t mask_offset = 0x1E90;
      err = dc1394_get_control_register(camera, mask_offset, &register_val);
      DC1394_ERR_RTN(err, "Failed to get mask value");
      uint32_t new_register_val = (register_val & 0xFFFFFFC0);
      if (atoi(argv[1]) == 1)
        new_register_val += 0x00000020;
      if (atoi(argv[2]) == 1)
        new_register_val += 0x00000010;
      if (atoi(argv[3]) == 1)
        new_register_val += 0x00000008;
      if (atoi(argv[4]) == 1)
        new_register_val += 0x00000004;
      if (atoi(argv[5]) == 1)
        new_register_val += 0x00000002;
      if (atoi(argv[6]) == 1)
        new_register_val += 0x00000001;

      err = dc1394_set_control_register(camera, mask_offset, new_register_val);
      DC1394_ERR_RTN(err, "Failed to set mask value");
      err = dc1394_get_control_register(camera, mask_offset, &register_val);
      DC1394_ERR_RTN(err, "Failed to get mask value");
      uint32_t mask_value = register_val & 0x3F; // get last 6 bits
      std::cout << "============AUTOEXPOSURE MASK VALUES============"
                << std::endl;
      std::cout << "MASK : Current: \t" << (std::bitset<6>)mask_value
                << std::endl;

    } else {
      print_help(argv);
    }
  }

  dc1394_camera_free(camera);
  dc1394_free(d);

  return 0;
}
