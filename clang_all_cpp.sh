#!/bin/bash

find src/ -iname *.h -o -iname *.cpp -o -iname *.hpp | xargs clang-format -i -style=file

echo "Done"

