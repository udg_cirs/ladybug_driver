# Linux driver for Point Grey's Ladybug 3 omnidirectional camera

All information about Point Grey's Ladybug 3 camera: [Point Grey's webpage](http://www.ptgrey.com/products/ladybug3/ladybug3_360_video_camera.asp)

##Requirements

* [libdc1394 library](http://damien.douxchamps.net/ieee1394/libdc1394/)

Optionally:

* [OpenCV Library](http://opencv.org/). You will need opencv 2.X.X if you want to use any online player utility

##Install

To compile:

```
mkdir tmp
cmake ..
make
```

##Run

Run any program from the bin directory.

For example:

```
cd bin
./ladybug_driver_raw dir_images 20

```

##List of utilities:

* **ladybug_driver_raw** - Driver for getting the images in RAW mode. FORMAT7 MODE0 from technical reference. Output 6 1616*1232 images every frame. Max: 5.2 fps

Usage :
```
./ladybug_driver_raw <Directory> <Time>
This will save all the frames (6 images per frame) in the specified directory ( must be new and relative path) during the Time (sec).
Usage :
```
* **ladybug_driver_raw_color_separated** - Driver for getting the images in RAW mode. FORMAT7 MODE1 from technical reference.  Output 6x4 808*616 images every frame. Max: 5.2 fps

Usage :
```
./ladybug_driver_raw_color_separated <Directory> <Time>
This will save all the frames (6x4 images per frame) in the specified directory ( must be new and relative path) during the Time (sec)
```
* **ladybug_driver_jpeg_color_separated** - Driver for getting the images in jpegs separated by channels. FORMAT7 MODE7 of technical reference. Output 6x4 
808*616 jpeg images every frame.  Max: 15 fps
Usage:
```
./ladybug_driver_jpeg_color_separated <Directory> <Time>
This will save all the frames (24 images per frame) in the specified directory ( must be new and relative path) during the Time (sec)
```
* **ladybug_view_channel** - Uses OpenCV to show online images from the camera (the 6 separately). Can show all cameras or just one. Can show coloured images or only a selected channel.
Usage:
```
./ladybug_view_channel <Camera> <Colour>
This will show in a new window the latest jpg image.Camera 0-5. To see al cameras: 6. If want colour set to 'All'. If set to 'Red', 'Green' or 'Blue' show only that channel. In case of green only one channel is shown.
```
* **ladybug_calibration** - Retrieve Point Grey's calibration file.
Usage:
```
./ladybug_calibration
```

##Troubleshooting:

If you experience problems getting the images correctly check this link:
[http://sourceforge.net/p/libdc1394/mailman/libdc1394-devel/thread/1376110457.2314.37.camel@moe/](http://sourceforge.net/p/libdc1394/mailman/libdc1394-devel/thread/1376110457.2314.37.camel@moe/)



